pimcore.plugin.mybundle = Class.create({
    initialize: function () {
        document.addEventListener(pimcore.events.preMenuBuild, this.preMenuBuild.bind(this));
    },

    preMenuBuild: function (e) {
        // the event contains the existing menu
        let menu = e.detail.menu;
        
        let items = [];
        // the property name is used as id with the prefix pimcore_menu_ in the html markup e.g. pimcore_menu_mybundle
        menu.mybundle = {
            label: t('myBundleLabel'),
            iconCls: 'pimcore_icon_apply',
            priority: 42,
            items: items,
            shadow: false,
            handler: this.openMyBundle.bind(this),
            noSubmenus: true,
            cls: "pimcore_navigation_flyout",
        };
    },

    openMyBundle: function(e) {
        try {
            pimcore.globalmanager.get("plugin_pimcore_mybundle").activate();
        } catch (e) {
            pimcore.globalmanager.add("plugin_pimcore_mybundle", new pimcore.plugin.mybundle());
        }
    },

    activate: function() {
        this.addGrid();
    },
    
    addGrid: function () {
        if (!this.panel) {
            this.panel = Ext.create('Ext.panel.Panel', {
                id: "pimcore_panel_system",
                title: t("Test Grid "),
                iconCls: "pimcore_icon_reports",
                border: false,
                layout: "fit",
                closable: true
            });
            this.panel.on("destroy", function () {
                pimcore.globalmanager.remove("panel_system");
                this.panel = false;
            }.bind(this));

            // var store = Ext.create('Ext.data.Store', {
                // fields: ['player', 'team', 'rating'],
                // autoLoad: true,
                // proxy: {
                //     type: 'ajax',
                //     url: '/custom/list',
                //     reader: {
                //         type: 'json',
                //         rootProperty: 'response'
                //     }
                // }
            // });

            this.filterField = new Ext.form.TextField({
                xtype: "textfield",
                width: 200,
                style: "margin: 0 10px 0 0;",
                enableKeyEvents: true,
                listeners: {
                    "keydown": function (field, key) {
                        // if (key.getKey() == key.ENTER) {
                        var input = field;
                        // var proxy = store.getProxy();
                        proxy.extraParams.filter = input.getValue();
                        // store.load();
                        // }
                    }.bind(this)
                }
            });

            this.layout = Ext.create('Ext.grid.Panel', {
                fullscreen: true,
                // tbar: toolbar,
                // title: 'Grid View ',
                // store: store,
                columns: [{
                    dataIndex: 'player',
                    text: 'Image',
                    width: 200,
                    filter: 'string',
                    renderer: function (value, meta) {
                        var renderHtml = "";
                        renderHtml = "<img src=" + value + " height='50' width='50' >";

                        return renderHtml;
                    }
                }, {
                    dataIndex: 'rating',
                    text: 'Rating',
                    filter: 'string',
                    sortable: true,
                    flex: 1
                }, {
                    dataIndex: 'team',
                    text: 'Team',
                    flex: 1,
                    filter: 'string',
                    sortable: true
                }
                ],

                tbar: {
                    cls: 'pimcore_main_toolbar',
                    items: [
                        {
                            text: t('GridView'),
                            handler: this.onAdd.bind(this),
                            iconCls: "pimcore_icon_add",
                            //disabled: !pimcore.settings['predefined-asset-metadata-writeable']
                        }, "->", {
                            text: t("filter") + "/" + t("search"),
                            xtype: "tbtext",
                            style: "margin: 0 10px 0 0;"
                        },
                        this.filterField
                    ]
                }
            });

            this.panel.add(this.layout);
            var tabPanel = Ext.getCmp("pimcore_panel_tabs");
            tabPanel.add(this.panel);
            tabPanel.setActiveItem(this.panel);
            pimcore.layout.refresh();
        } else {
            var tabPanel = Ext.getCmp("pimcore_panel_tabs");
            tabPanel.setActiveItem("pimcore_panel_system");
        }
    },

    onAdd: function (btn, ev) {
        this.grid.store.insert(0,{
            name: t('/')
        });

        this.updateRows();
    }
    
});

var myBundle = new pimcore.plugin.mybundle();
