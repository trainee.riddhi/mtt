<?php

/**
 * Inheritance: no
 * Variants: no
 *
 * Fields Summary:
 * - size [manyToOneRelation]
 * - sizeCode [manyToOneRelation]
 * - sizeCodeName [manyToOneRelation]
 * - collection [input]
 * - printFamily [manyToOneRelation]
 * - colorFamily [select]
 * - nrfColorName [select]
 * - nrfColorCode [select]
 * - accountCategory [select]
 * - subCategory [select]
 * - classification [select]
 * - season [select]
 * - year [input]
 * - gender [select]
 * - ageRange [select]
 * - materials [select]
 * - slvLength [select]
 * - bottomLength [select]
 * - width [input]
 * - weight [input]
 * - closure [select]
 * - productCode [input]
 * - productName [input]
 * - styleCode [manyToOneRelation]
 * - colorCode [manyToOneRelation]
 * - productCodeOther [input]
 * - variantName [input]
 * - templateName [input]
 * - styleDescription [textarea]
 * - department [select]
 * - active [checkbox]
 * - launchDate [date]
 * - nuCustomerGroups [select]
 * - weightUomName [select]
 * - costPriceMethod [select]
 * - customsDescription [textarea]
 * - datetimeAdded [date]
 * - preBook [booleanSelect]
 * - priceMethod [manyToOneRelation]
 * - listPrice [manyToManyRelation]
 * - currentPrice [input]
 * - brand [manyToOneRelation]
 * - channel [select]
 * - productType [input]
 * - height [input]
 * - length [input]
 * - dimensionUomName [select]
 * - weightUom [select]
 * - upc [input]
 * - division [select]
 * - productProperty [select]
 * - costPrice [input]
 * - shipDate [date]
 * - materialFamily [select]
 * - supplierName [select]
 * - license [select]
 * - hsCode [select]
 * - lifecycle [select]
 */

return Pimcore\Model\DataObject\ClassDefinition::__set_state(array(
   'dao' => NULL,
   'id' => 'product',
   'name' => 'Product',
   'title' => '',
   'description' => '',
   'creationDate' => NULL,
   'modificationDate' => 1692775832,
   'userOwner' => 2,
   'userModification' => 2,
   'parentClass' => '',
   'implementsInterfaces' => '',
   'listingParentClass' => '',
   'useTraits' => '',
   'listingUseTraits' => '',
   'encryption' => false,
   'encryptedTables' => 
  array (
  ),
   'allowInherit' => false,
   'allowVariants' => false,
   'showVariants' => false,
   'layoutDefinitions' => 
  Pimcore\Model\DataObject\ClassDefinition\Layout\Panel::__set_state(array(
     'name' => 'pimcore_root',
     'type' => NULL,
     'region' => NULL,
     'title' => NULL,
     'width' => 0,
     'height' => 0,
     'collapsible' => false,
     'collapsed' => false,
     'bodyStyle' => NULL,
     'datatype' => 'layout',
     'children' => 
    array (
      0 => 
      Pimcore\Model\DataObject\ClassDefinition\Layout\Tabpanel::__set_state(array(
         'name' => 'Layout',
         'type' => NULL,
         'region' => NULL,
         'title' => '',
         'width' => '',
         'height' => '',
         'collapsible' => false,
         'collapsed' => false,
         'bodyStyle' => '',
         'datatype' => 'layout',
         'children' => 
        array (
          0 => 
          Pimcore\Model\DataObject\ClassDefinition\Layout\Panel::__set_state(array(
             'name' => 'Basic',
             'type' => NULL,
             'region' => NULL,
             'title' => 'Basic',
             'width' => '',
             'height' => '',
             'collapsible' => false,
             'collapsed' => false,
             'bodyStyle' => '',
             'datatype' => 'layout',
             'children' => 
            array (
              0 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\ManyToOneRelation::__set_state(array(
                 'name' => 'size',
                 'title' => 'Size',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => true,
                 'invisible' => false,
                 'visibleGridView' => true,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'classes' => 
                array (
                  0 => 
                  array (
                    'classes' => 'Size',
                  ),
                ),
                 'displayMode' => 'grid',
                 'pathFormatterClass' => '',
                 'assetInlineDownloadAllowed' => false,
                 'assetUploadPath' => '',
                 'allowToClearRelation' => true,
                 'objectsAllowed' => true,
                 'assetsAllowed' => false,
                 'assetTypes' => 
                array (
                ),
                 'documentsAllowed' => false,
                 'documentTypes' => 
                array (
                ),
                 'width' => '',
              )),
              1 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\ManyToOneRelation::__set_state(array(
                 'name' => 'sizeCode',
                 'title' => 'Size Code',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => true,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'classes' => 
                array (
                  0 => 
                  array (
                    'classes' => 'SizeCode',
                  ),
                ),
                 'displayMode' => 'grid',
                 'pathFormatterClass' => '',
                 'assetInlineDownloadAllowed' => false,
                 'assetUploadPath' => '',
                 'allowToClearRelation' => true,
                 'objectsAllowed' => true,
                 'assetsAllowed' => false,
                 'assetTypes' => 
                array (
                ),
                 'documentsAllowed' => false,
                 'documentTypes' => 
                array (
                ),
                 'width' => '',
              )),
              2 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\ManyToOneRelation::__set_state(array(
                 'name' => 'sizeCodeName',
                 'title' => 'Size Code Name',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => true,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'classes' => 
                array (
                  0 => 
                  array (
                    'classes' => 'SizeCodeName',
                  ),
                ),
                 'displayMode' => 'grid',
                 'pathFormatterClass' => '',
                 'assetInlineDownloadAllowed' => false,
                 'assetUploadPath' => '',
                 'allowToClearRelation' => true,
                 'objectsAllowed' => true,
                 'assetsAllowed' => false,
                 'assetTypes' => 
                array (
                ),
                 'documentsAllowed' => false,
                 'documentTypes' => 
                array (
                ),
                 'width' => '',
              )),
              3 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                 'name' => 'collection',
                 'title' => 'Collection',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => true,
                 'visibleSearch' => true,
                 'blockedVarsForExport' => 
                array (
                ),
                 'defaultValue' => NULL,
                 'columnLength' => 190,
                 'regex' => '',
                 'regexFlags' => 
                array (
                ),
                 'unique' => false,
                 'showCharCount' => false,
                 'width' => '',
                 'defaultValueGenerator' => '',
              )),
              4 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\ManyToOneRelation::__set_state(array(
                 'name' => 'printFamily',
                 'title' => 'Print Family',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => true,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'classes' => 
                array (
                  0 => 
                  array (
                    'classes' => 'PrintFamily',
                  ),
                ),
                 'displayMode' => 'grid',
                 'pathFormatterClass' => '',
                 'assetInlineDownloadAllowed' => false,
                 'assetUploadPath' => '',
                 'allowToClearRelation' => true,
                 'objectsAllowed' => true,
                 'assetsAllowed' => false,
                 'assetTypes' => 
                array (
                ),
                 'documentsAllowed' => false,
                 'documentTypes' => 
                array (
                ),
                 'width' => '',
              )),
              5 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                 'name' => 'colorFamily',
                 'title' => 'Color Family',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'options' => 
                array (
                  0 => 
                  array (
                    'key' => 'BEIZE',
                    'value' => 'BEIZE',
                  ),
                  1 => 
                  array (
                    'key' => 'BLUE',
                    'value' => 'BLUE',
                  ),
                  2 => 
                  array (
                    'key' => 'PINK',
                    'value' => 'PINK',
                  ),
                  3 => 
                  array (
                    'key' => 'WHITE',
                    'value' => 'WHITE',
                  ),
                  4 => 
                  array (
                    'key' => 'ORANGE',
                    'value' => 'ORANGE',
                  ),
                  5 => 
                  array (
                    'key' => 'BLACK',
                    'value' => 'BLACK',
                  ),
                  6 => 
                  array (
                    'key' => 'BROWN',
                    'value' => 'BROWN',
                  ),
                  7 => 
                  array (
                    'key' => 'GREEN',
                    'value' => 'GREEN',
                  ),
                  8 => 
                  array (
                    'key' => 'GREY',
                    'value' => 'GREY',
                  ),
                  9 => 
                  array (
                    'key' => 'MULTI',
                    'value' => 'MULTI',
                  ),
                  10 => 
                  array (
                    'key' => 'PURPLE',
                    'value' => 'PURPLE',
                  ),
                  11 => 
                  array (
                    'key' => 'RED',
                    'value' => 'RED',
                  ),
                  12 => 
                  array (
                    'key' => 'YELLOW',
                    'value' => 'YELLOW',
                  ),
                ),
                 'defaultValue' => '',
                 'optionsProviderClass' => '',
                 'optionsProviderData' => '',
                 'columnLength' => 190,
                 'dynamicOptions' => false,
                 'defaultValueGenerator' => '',
                 'width' => '',
              )),
              6 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                 'name' => 'nrfColorName',
                 'title' => 'Nrf Color Name',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => true,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'options' => 
                array (
                  0 => 
                  array (
                    'key' => 'NAVY',
                    'value' => 'NAVY',
                  ),
                  1 => 
                  array (
                    'key' => 'TURQUOISE',
                    'value' => 'TURQUOISE',
                  ),
                  2 => 
                  array (
                    'key' => 'WHITE',
                    'value' => 'WHITE',
                  ),
                  3 => 
                  array (
                    'key' => 'DUSTY ROSE',
                    'value' => 'DUSTY ROSE',
                  ),
                  4 => 
                  array (
                    'key' => 'BUBBLEGUM',
                    'value' => 'BUBBLEGUM',
                  ),
                  5 => 
                  array (
                    'key' => 'OXFORD',
                    'value' => 'OXFORD',
                  ),
                ),
                 'defaultValue' => '',
                 'optionsProviderClass' => '',
                 'optionsProviderData' => '',
                 'columnLength' => 190,
                 'dynamicOptions' => false,
                 'defaultValueGenerator' => '',
                 'width' => '',
              )),
              7 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                 'name' => 'nrfColorCode',
                 'title' => 'Nrf Color Code',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => true,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'options' => 
                array (
                  0 => 
                  array (
                    'key' => '410',
                    'value' => '410',
                  ),
                  1 => 
                  array (
                    'key' => '690',
                    'value' => '690',
                  ),
                  2 => 
                  array (
                    'key' => '288',
                    'value' => '288',
                  ),
                  3 => 
                  array (
                    'key' => '292',
                    'value' => '292',
                  ),
                  4 => 
                  array (
                    'key' => '302',
                    'value' => '302',
                  ),
                  5 => 
                  array (
                    'key' => '3',
                    'value' => '3',
                  ),
                ),
                 'defaultValue' => '',
                 'optionsProviderClass' => '',
                 'optionsProviderData' => '',
                 'columnLength' => 190,
                 'dynamicOptions' => false,
                 'defaultValueGenerator' => '',
                 'width' => '',
              )),
              8 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                 'name' => 'accountCategory',
                 'title' => 'Account Category',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => true,
                 'visibleSearch' => true,
                 'blockedVarsForExport' => 
                array (
                ),
                 'options' => 
                array (
                  0 => 
                  array (
                    'key' => 'DRESS',
                    'value' => 'DRESS',
                  ),
                  1 => 
                  array (
                    'key' => 'BODYSUIT',
                    'value' => 'BODYSUIT',
                  ),
                  2 => 
                  array (
                    'key' => 'OTHER',
                    'value' => 'OTHER',
                  ),
                  3 => 
                  array (
                    'key' => 'GIFT BOX',
                    'value' => 'GIFT BOX',
                  ),
                  4 => 
                  array (
                    'key' => 'HEADWEAR',
                    'value' => 'HEADWEAR',
                  ),
                  5 => 
                  array (
                    'key' => 'Parent product category  / ONE PIECE',
                    'value' => 'Parent product category  / ONE PIECE',
                  ),
                ),
                 'defaultValue' => '',
                 'optionsProviderClass' => '',
                 'optionsProviderData' => '',
                 'columnLength' => 190,
                 'dynamicOptions' => false,
                 'defaultValueGenerator' => '',
                 'width' => '',
              )),
              9 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                 'name' => 'subCategory',
                 'title' => 'Sub Category',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => true,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'options' => 
                array (
                  0 => 
                  array (
                    'key' => 'SHIP',
                    'value' => 'SHIP',
                  ),
                  1 => 
                  array (
                    'key' => 'OTHER',
                    'value' => 'OTHER',
                  ),
                  2 => 
                  array (
                    'key' => 'GIFT BOX',
                    'value' => 'GIFT BOX',
                  ),
                  3 => 
                  array (
                    'key' => 'GIFT CARD',
                    'value' => 'GIFT CARD',
                  ),
                  4 => 
                  array (
                    'key' => 'PACKAGING',
                    'value' => 'PACKAGING',
                  ),
                  5 => 
                  array (
                    'key' => 'FOOTIES',
                    'value' => 'FOOTIES',
                  ),
                ),
                 'defaultValue' => '',
                 'optionsProviderClass' => '',
                 'optionsProviderData' => '',
                 'columnLength' => 190,
                 'dynamicOptions' => false,
                 'defaultValueGenerator' => '',
                 'width' => '',
              )),
              10 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                 'name' => 'classification',
                 'title' => 'Classification',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => true,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'options' => 
                array (
                  0 => 
                  array (
                    'key' => 'SEASONAL FASHION',
                    'value' => 'SEASONAL FASHION',
                  ),
                  1 => 
                  array (
                    'key' => 'CORE ALL YEAR',
                    'value' => 'CORE ALL YEAR',
                  ),
                  2 => 
                  array (
                    'key' => 'CORE FALL/WINTER',
                    'value' => 'CORE FALL/WINTER',
                  ),
                  3 => 
                  array (
                    'key' => 'SEASONAL',
                    'value' => 'SEASONAL',
                  ),
                  4 => 
                  array (
                    'key' => 'CORE',
                    'value' => 'CORE',
                  ),
                  5 => 
                  array (
                    'key' => 'N/A',
                    'value' => 'N/A',
                  ),
                ),
                 'defaultValue' => '',
                 'optionsProviderClass' => '',
                 'optionsProviderData' => '',
                 'columnLength' => 190,
                 'dynamicOptions' => false,
                 'defaultValueGenerator' => '',
                 'width' => '',
              )),
              11 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                 'name' => 'season',
                 'title' => 'Season',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => true,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'options' => 
                array (
                  0 => 
                  array (
                    'key' => 'CORE',
                    'value' => 'CORE',
                  ),
                  1 => 
                  array (
                    'key' => 'SUMMER 2018',
                    'value' => 'SUMMER 2018',
                  ),
                  2 => 
                  array (
                    'key' => 'DISCONTINUED',
                    'value' => 'DISCONTINUED',
                  ),
                  3 => 
                  array (
                    'key' => 'SPRING 2019',
                    'value' => 'SPRING 2019',
                  ),
                  4 => 
                  array (
                    'key' => 'HALLOWEEN 2020',
                    'value' => 'HALLOWEEN 2020',
                  ),
                ),
                 'defaultValue' => '',
                 'optionsProviderClass' => '',
                 'optionsProviderData' => '',
                 'columnLength' => 190,
                 'dynamicOptions' => false,
                 'defaultValueGenerator' => '',
                 'width' => '',
              )),
              12 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                 'name' => 'year',
                 'title' => 'Year',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'defaultValue' => NULL,
                 'columnLength' => 190,
                 'regex' => '',
                 'regexFlags' => 
                array (
                ),
                 'unique' => false,
                 'showCharCount' => false,
                 'width' => '',
                 'defaultValueGenerator' => '',
              )),
              13 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                 'name' => 'gender',
                 'title' => 'Gender',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'options' => 
                array (
                  0 => 
                  array (
                    'key' => 'WOMEN',
                    'value' => 'WOMEN',
                  ),
                  1 => 
                  array (
                    'key' => 'UNISEX',
                    'value' => 'UNISEX',
                  ),
                  2 => 
                  array (
                    'key' => 'PETS',
                    'value' => 'PETS',
                  ),
                  3 => 
                  array (
                    'key' => 'NEUTRAL',
                    'value' => 'NEUTRAL',
                  ),
                  4 => 
                  array (
                    'key' => 'N/A',
                    'value' => 'N/A',
                  ),
                  5 => 
                  array (
                    'key' => 'MEN',
                    'value' => 'MEN',
                  ),
                  6 => 
                  array (
                    'key' => 'GIRLS',
                    'value' => 'GIRLS',
                  ),
                  7 => 
                  array (
                    'key' => 'GIRL',
                    'value' => 'GIRL',
                  ),
                  8 => 
                  array (
                    'key' => 'BOYS',
                    'value' => 'BOYS',
                  ),
                  9 => 
                  array (
                    'key' => 'BOY',
                    'value' => 'BOY',
                  ),
                  10 => 
                  array (
                    'key' => 'BABY UNISEX',
                    'value' => 'BABY UNISEX',
                  ),
                  11 => 
                  array (
                    'key' => 'BABY GIRL',
                    'value' => 'BABY GIRL',
                  ),
                  12 => 
                  array (
                    'key' => 'BABY BOY',
                    'value' => 'BABY BOY',
                  ),
                ),
                 'defaultValue' => '',
                 'optionsProviderClass' => '',
                 'optionsProviderData' => '',
                 'columnLength' => 190,
                 'dynamicOptions' => false,
                 'defaultValueGenerator' => '',
                 'width' => '',
              )),
              14 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                 'name' => 'ageRange',
                 'title' => 'Age Range',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'options' => 
                array (
                  0 => 
                  array (
                    'key' => 'ADULTS',
                    'value' => 'ADULTS',
                  ),
                  1 => 
                  array (
                    'key' => 'ALL',
                    'value' => 'ALL',
                  ),
                  2 => 
                  array (
                    'key' => 'INFANT',
                    'value' => 'INFANT',
                  ),
                  3 => 
                  array (
                    'key' => 'PET',
                    'value' => 'PET',
                  ),
                  4 => 
                  array (
                    'key' => 'TODDLER',
                    'value' => 'TODDLER',
                  ),
                  5 => 
                  array (
                    'key' => 'YOUTH',
                    'value' => 'YOUTH',
                  ),
                ),
                 'defaultValue' => '',
                 'optionsProviderClass' => '',
                 'optionsProviderData' => '',
                 'columnLength' => 190,
                 'dynamicOptions' => false,
                 'defaultValueGenerator' => '',
                 'width' => '',
              )),
              15 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                 'name' => 'materials',
                 'title' => 'Materials',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'options' => 
                array (
                  0 => 
                  array (
                    'key' => 'TERRY(100 COTTON)',
                    'value' => 'TERRY(100 COTTON)',
                  ),
                  1 => 
                  array (
                    'key' => 'POPLIN(100 COTTON)',
                    'value' => 'POPLIN(100 COTTON)',
                  ),
                  2 => 
                  array (
                    'key' => 'POINTELLE(100 COTTON)',
                    'value' => 'POINTELLE(100 COTTON)',
                  ),
                  3 => 
                  array (
                    'key' => 'NYLON/SPANDEX(80/20)',
                    'value' => 'NYLON/SPANDEX(80/20)',
                  ),
                  4 => 
                  array (
                    'key' => 'JACQUARD/BAMBOO/COTTON(73/27)',
                    'value' => 'JACQUARD/BAMBOO/COTTON(73/27)',
                  ),
                  5 => 
                  array (
                    'key' => 'FRENCH TERRY(100 COTTON)',
                    'value' => 'FRENCH TERRY(100 COTTON)',
                  ),
                  6 => 
                  array (
                    'key' => 'CANVAS(90 POLYESTER 10 PU)',
                    'value' => 'CANVAS(90 POLYESTER 10 PU)',
                  ),
                  7 => 
                  array (
                    'key' => 'BAMBOO/SPANDEX(95/5)',
                    'value' => 'BAMBOO/SPANDEX(95/5)',
                  ),
                  8 => 
                  array (
                    'key' => 'BAMBOO/COTTON(60/40)',
                    'value' => 'BAMBOO/COTTON(60/40)',
                  ),
                  9 => 
                  array (
                    'key' => '100 RAYON',
                    'value' => '100 RAYON',
                  ),
                  10 => 
                  array (
                    'key' => '100 POLYESTER',
                    'value' => '100 POLYESTER',
                  ),
                  11 => 
                  array (
                    'key' => '(90 POLYESTER 10 NYLON)',
                    'value' => '(90 POLYESTER 10 NYLON)',
                  ),
                ),
                 'defaultValue' => '',
                 'optionsProviderClass' => '',
                 'optionsProviderData' => '',
                 'columnLength' => 190,
                 'dynamicOptions' => false,
                 'defaultValueGenerator' => '',
                 'width' => '',
              )),
              16 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                 'name' => 'slvLength',
                 'title' => 'Slv Length',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'options' => 
                array (
                  0 => 
                  array (
                    'key' => '3/4',
                    'value' => '3/4',
                  ),
                  1 => 
                  array (
                    'key' => 'CAP',
                    'value' => 'CAP',
                  ),
                  2 => 
                  array (
                    'key' => 'LONG',
                    'value' => 'LONG',
                  ),
                  3 => 
                  array (
                    'key' => 'N/A',
                    'value' => 'N/A',
                  ),
                  4 => 
                  array (
                    'key' => 'SHORT',
                    'value' => 'SHORT',
                  ),
                  5 => 
                  array (
                    'key' => 'SLEEVELESS',
                    'value' => 'SLEEVLESS',
                  ),
                ),
                 'defaultValue' => '',
                 'optionsProviderClass' => '',
                 'optionsProviderData' => '',
                 'columnLength' => 190,
                 'dynamicOptions' => false,
                 'defaultValueGenerator' => '',
                 'width' => '',
              )),
              17 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                 'name' => 'bottomLength',
                 'title' => 'Bottom Length',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'options' => 
                array (
                  0 => 
                  array (
                    'key' => 'BUMMIES',
                    'value' => 'BUMMIES',
                  ),
                  1 => 
                  array (
                    'key' => 'CONVERTIBLE',
                    'value' => 'CONVERTIBLE',
                  ),
                  2 => 
                  array (
                    'key' => 'FOOTIE',
                    'value' => 'FOOTIE',
                  ),
                  3 => 
                  array (
                    'key' => 'LONG',
                    'value' => 'LONG',
                  ),
                  4 => 
                  array (
                    'key' => 'MIDI',
                    'value' => 'MIDI',
                  ),
                  5 => 
                  array (
                    'key' => 'N/A',
                    'value' => 'N/A',
                  ),
                  6 => 
                  array (
                    'key' => 'SHORT',
                    'value' => 'SHORT',
                  ),
                ),
                 'defaultValue' => '',
                 'optionsProviderClass' => '',
                 'optionsProviderData' => '',
                 'columnLength' => 190,
                 'dynamicOptions' => false,
                 'defaultValueGenerator' => '',
                 'width' => '',
              )),
              18 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                 'name' => 'width',
                 'title' => 'Width',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => true,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'defaultValue' => NULL,
                 'columnLength' => 190,
                 'regex' => '',
                 'regexFlags' => 
                array (
                ),
                 'unique' => false,
                 'showCharCount' => false,
                 'width' => '',
                 'defaultValueGenerator' => '',
              )),
              19 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                 'name' => 'weight',
                 'title' => 'Weight',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => true,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'defaultValue' => NULL,
                 'columnLength' => 190,
                 'regex' => '',
                 'regexFlags' => 
                array (
                ),
                 'unique' => false,
                 'showCharCount' => false,
                 'width' => '',
                 'defaultValueGenerator' => '',
              )),
              20 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                 'name' => 'closure',
                 'title' => 'Closure',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'options' => 
                array (
                  0 => 
                  array (
                    'key' => 'BELTED',
                    'value' => 'BELTED',
                  ),
                  1 => 
                  array (
                    'key' => 'BUTTON',
                    'value' => 'BUTTON',
                  ),
                  2 => 
                  array (
                    'key' => 'HOOK',
                    'value' => 'HOOK',
                  ),
                  3 => 
                  array (
                    'key' => 'N/A',
                    'value' => 'N/A',
                  ),
                  4 => 
                  array (
                    'key' => 'SNAP',
                    'value' => 'SNAP',
                  ),
                  5 => 
                  array (
                    'key' => 'TIE',
                    'value' => 'TIE',
                  ),
                  6 => 
                  array (
                    'key' => 'ZIP',
                    'value' => 'ZIP',
                  ),
                ),
                 'defaultValue' => '',
                 'optionsProviderClass' => '',
                 'optionsProviderData' => '',
                 'columnLength' => 190,
                 'dynamicOptions' => false,
                 'defaultValueGenerator' => '',
                 'width' => '',
              )),
            ),
             'locked' => false,
             'blockedVarsForExport' => 
            array (
            ),
             'fieldtype' => 'panel',
             'layout' => NULL,
             'border' => false,
             'icon' => '',
             'labelWidth' => 100,
             'labelAlign' => 'left',
          )),
          1 => 
          Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldset::__set_state(array(
             'name' => 'fullFill',
             'type' => NULL,
             'region' => NULL,
             'title' => 'Fullfill',
             'width' => '',
             'height' => '',
             'collapsible' => false,
             'collapsed' => false,
             'bodyStyle' => '',
             'datatype' => 'layout',
             'children' => 
            array (
              0 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                 'name' => 'productCode',
                 'title' => 'product Code ',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => true,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'defaultValue' => NULL,
                 'columnLength' => 190,
                 'regex' => '',
                 'regexFlags' => 
                array (
                ),
                 'unique' => false,
                 'showCharCount' => false,
                 'width' => '',
                 'defaultValueGenerator' => '',
              )),
              1 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                 'name' => 'productName',
                 'title' => 'Product Name',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => true,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'defaultValue' => NULL,
                 'columnLength' => 190,
                 'regex' => '',
                 'regexFlags' => 
                array (
                ),
                 'unique' => false,
                 'showCharCount' => false,
                 'width' => '',
                 'defaultValueGenerator' => '',
              )),
              2 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\ManyToOneRelation::__set_state(array(
                 'name' => 'styleCode',
                 'title' => 'Style Code',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => true,
                 'invisible' => false,
                 'visibleGridView' => true,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'classes' => 
                array (
                  0 => 
                  array (
                    'classes' => 'StyleCode',
                  ),
                ),
                 'displayMode' => 'grid',
                 'pathFormatterClass' => '',
                 'assetInlineDownloadAllowed' => false,
                 'assetUploadPath' => '',
                 'allowToClearRelation' => true,
                 'objectsAllowed' => true,
                 'assetsAllowed' => false,
                 'assetTypes' => 
                array (
                ),
                 'documentsAllowed' => false,
                 'documentTypes' => 
                array (
                ),
                 'width' => '',
              )),
              3 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\ManyToOneRelation::__set_state(array(
                 'name' => 'colorCode',
                 'title' => 'Color Code',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => true,
                 'invisible' => false,
                 'visibleGridView' => true,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'classes' => 
                array (
                  0 => 
                  array (
                    'classes' => 'ColorCode',
                  ),
                ),
                 'displayMode' => 'grid',
                 'pathFormatterClass' => '',
                 'assetInlineDownloadAllowed' => false,
                 'assetUploadPath' => '',
                 'allowToClearRelation' => true,
                 'objectsAllowed' => true,
                 'assetsAllowed' => false,
                 'assetTypes' => 
                array (
                ),
                 'documentsAllowed' => false,
                 'documentTypes' => 
                array (
                ),
                 'width' => '',
              )),
              4 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                 'name' => 'productCodeOther',
                 'title' => 'Product Code Other (colorCode + styleCode)',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => true,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'defaultValue' => NULL,
                 'columnLength' => 190,
                 'regex' => '',
                 'regexFlags' => 
                array (
                ),
                 'unique' => false,
                 'showCharCount' => false,
                 'width' => '',
                 'defaultValueGenerator' => '',
              )),
              5 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                 'name' => 'variantName',
                 'title' => 'Variant Name',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => true,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'defaultValue' => NULL,
                 'columnLength' => 190,
                 'regex' => '',
                 'regexFlags' => 
                array (
                ),
                 'unique' => false,
                 'showCharCount' => false,
                 'width' => '',
                 'defaultValueGenerator' => '',
              )),
              6 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                 'name' => 'templateName',
                 'title' => 'Template Name',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => true,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'defaultValue' => NULL,
                 'columnLength' => 190,
                 'regex' => '',
                 'regexFlags' => 
                array (
                ),
                 'unique' => false,
                 'showCharCount' => false,
                 'width' => '',
                 'defaultValueGenerator' => '',
              )),
              7 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Textarea::__set_state(array(
                 'name' => 'styleDescription',
                 'title' => 'Style Description',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => true,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'maxLength' => NULL,
                 'showCharCount' => false,
                 'excludeFromSearchIndex' => false,
                 'height' => '',
                 'width' => '',
              )),
              8 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                 'name' => 'department',
                 'title' => 'Department',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'options' => 
                array (
                  0 => 
                  array (
                    'key' => 'APPAREL',
                    'value' => 'APPAREL',
                  ),
                  1 => 
                  array (
                    'key' => 'ACCESSORIES',
                    'value' => 'ACCESSORIES',
                  ),
                  2 => 
                  array (
                    'key' => 'GIFT',
                    'value' => 'GIFT',
                  ),
                  3 => 
                  array (
                    'key' => 'HOME',
                    'value' => 'HOME',
                  ),
                  4 => 
                  array (
                    'key' => 'MYSTERY BOX',
                    'value' => 'MYSTERY BOX',
                  ),
                  5 => 
                  array (
                    'key' => 'NON-SALEABLE',
                    'value' => 'NON-SALEABLE',
                  ),
                  6 => 
                  array (
                    'key' => 'SWIM',
                    'value' => 'SWIM',
                  ),
                ),
                 'defaultValue' => '',
                 'optionsProviderClass' => '',
                 'optionsProviderData' => '',
                 'columnLength' => 190,
                 'dynamicOptions' => false,
                 'defaultValueGenerator' => '',
                 'width' => '',
              )),
              9 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Checkbox::__set_state(array(
                 'name' => 'active',
                 'title' => 'Active',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => true,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'defaultValue' => NULL,
                 'defaultValueGenerator' => '',
              )),
              10 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Date::__set_state(array(
                 'name' => 'launchDate',
                 'title' => 'Launch Date',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => true,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'defaultValue' => NULL,
                 'useCurrentDate' => false,
                 'columnType' => 'bigint(20)',
                 'defaultValueGenerator' => '',
              )),
              11 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                 'name' => 'nuCustomerGroups',
                 'title' => 'Nu Customer Groups',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => true,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'options' => 
                array (
                  0 => 
                  array (
                    'key' => 'ALL',
                    'value' => 'ALL',
                  ),
                  1 => 
                  array (
                    'key' => 'NONE',
                    'value' => 'NONE',
                  ),
                ),
                 'defaultValue' => '',
                 'optionsProviderClass' => '',
                 'optionsProviderData' => '',
                 'columnLength' => 190,
                 'dynamicOptions' => false,
                 'defaultValueGenerator' => '',
                 'width' => '',
              )),
              12 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                 'name' => 'weightUomName',
                 'title' => 'Weight Uom Name',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => true,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'options' => 
                array (
                  0 => 
                  array (
                    'key' => 'Ounce',
                    'value' => 'Ounce',
                  ),
                  1 => 
                  array (
                    'key' => 'Pound',
                    'value' => 'Pound',
                  ),
                ),
                 'defaultValue' => '',
                 'optionsProviderClass' => '',
                 'optionsProviderData' => '',
                 'columnLength' => 190,
                 'dynamicOptions' => false,
                 'defaultValueGenerator' => '',
                 'width' => '',
              )),
              13 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                 'name' => 'costPriceMethod',
                 'title' => 'Cost Price Method',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => true,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'options' => 
                array (
                  0 => 
                  array (
                    'key' => 'average',
                    'value' => 'average',
                  ),
                  1 => 
                  array (
                    'key' => 'fixed',
                    'value' => 'fixed',
                  ),
                ),
                 'defaultValue' => '',
                 'optionsProviderClass' => '',
                 'optionsProviderData' => '',
                 'columnLength' => 190,
                 'dynamicOptions' => false,
                 'defaultValueGenerator' => '',
                 'width' => '',
              )),
              14 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Textarea::__set_state(array(
                 'name' => 'customsDescription',
                 'title' => 'Customs Description',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => true,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'maxLength' => NULL,
                 'showCharCount' => false,
                 'excludeFromSearchIndex' => false,
                 'height' => '',
                 'width' => '',
              )),
              15 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Date::__set_state(array(
                 'name' => 'datetimeAdded',
                 'title' => 'Datetime Added',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'defaultValue' => NULL,
                 'useCurrentDate' => false,
                 'columnType' => 'bigint(20)',
                 'defaultValueGenerator' => '',
              )),
              16 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\BooleanSelect::__set_state(array(
                 'name' => 'preBook',
                 'title' => 'Pre Book',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => true,
                 'blockedVarsForExport' => 
                array (
                ),
                 'yesLabel' => 'true',
                 'noLabel' => 'false',
                 'emptyLabel' => 'empty',
                 'options' => 
                array (
                  0 => 
                  array (
                    'key' => 'empty',
                    'value' => 0,
                  ),
                  1 => 
                  array (
                    'key' => 'true',
                    'value' => 1,
                  ),
                  2 => 
                  array (
                    'key' => 'false',
                    'value' => -1,
                  ),
                ),
                 'width' => '',
              )),
              17 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\ManyToOneRelation::__set_state(array(
                 'name' => 'priceMethod',
                 'title' => 'Price Method',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => true,
                 'invisible' => false,
                 'visibleGridView' => true,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'classes' => 
                array (
                  0 => 
                  array (
                    'classes' => 'PriceMethod',
                  ),
                ),
                 'displayMode' => 'grid',
                 'pathFormatterClass' => '',
                 'assetInlineDownloadAllowed' => false,
                 'assetUploadPath' => '',
                 'allowToClearRelation' => true,
                 'objectsAllowed' => true,
                 'assetsAllowed' => false,
                 'assetTypes' => 
                array (
                ),
                 'documentsAllowed' => false,
                 'documentTypes' => 
                array (
                ),
                 'width' => '',
              )),
              18 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\ManyToManyRelation::__set_state(array(
                 'name' => 'listPrice',
                 'title' => 'List Price',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => true,
                 'invisible' => false,
                 'visibleGridView' => true,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'classes' => 
                array (
                  0 => 
                  array (
                    'classes' => 'ListPrice',
                  ),
                ),
                 'displayMode' => NULL,
                 'pathFormatterClass' => '',
                 'maxItems' => NULL,
                 'assetInlineDownloadAllowed' => false,
                 'assetUploadPath' => '',
                 'allowToClearRelation' => true,
                 'objectsAllowed' => true,
                 'assetsAllowed' => false,
                 'assetTypes' => 
                array (
                ),
                 'documentsAllowed' => false,
                 'documentTypes' => 
                array (
                ),
                 'enableTextSelection' => false,
                 'width' => '',
                 'height' => '',
              )),
              19 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                 'name' => 'currentPrice',
                 'title' => 'Current Price',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'defaultValue' => NULL,
                 'columnLength' => 190,
                 'regex' => '',
                 'regexFlags' => 
                array (
                ),
                 'unique' => false,
                 'showCharCount' => false,
                 'width' => '',
                 'defaultValueGenerator' => '',
              )),
              20 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\ManyToOneRelation::__set_state(array(
                 'name' => 'brand',
                 'title' => 'Brand',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => true,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'classes' => 
                array (
                  0 => 
                  array (
                    'classes' => 'Brand',
                  ),
                ),
                 'displayMode' => 'grid',
                 'pathFormatterClass' => '',
                 'assetInlineDownloadAllowed' => false,
                 'assetUploadPath' => '',
                 'allowToClearRelation' => true,
                 'objectsAllowed' => true,
                 'assetsAllowed' => false,
                 'assetTypes' => 
                array (
                ),
                 'documentsAllowed' => false,
                 'documentTypes' => 
                array (
                ),
                 'width' => '',
              )),
              21 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                 'name' => 'channel',
                 'title' => 'Channel',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'options' => 
                array (
                  0 => 
                  array (
                    'key' => 'AMAZON',
                    'value' => 'AMAZON',
                  ),
                  1 => 
                  array (
                    'key' => 'MULTICHANNEL',
                    'value' => 'MULTICHANNEL',
                  ),
                  2 => 
                  array (
                    'key' => 'SHOPIFY',
                    'value' => 'SHOPIFY',
                  ),
                  3 => 
                  array (
                    'key' => 'WHOLESALE',
                    'value' => 'WHOLESALE',
                  ),
                ),
                 'defaultValue' => '',
                 'optionsProviderClass' => '',
                 'optionsProviderData' => '',
                 'columnLength' => 190,
                 'dynamicOptions' => false,
                 'defaultValueGenerator' => '',
                 'width' => '',
              )),
              22 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                 'name' => 'productType',
                 'title' => 'Type',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'defaultValue' => NULL,
                 'columnLength' => 190,
                 'regex' => '',
                 'regexFlags' => 
                array (
                ),
                 'unique' => false,
                 'showCharCount' => false,
                 'width' => '',
                 'defaultValueGenerator' => '',
              )),
            ),
             'locked' => false,
             'blockedVarsForExport' => 
            array (
            ),
             'fieldtype' => 'fieldset',
             'labelWidth' => 100,
             'labelAlign' => 'left',
          )),
          2 => 
          Pimcore\Model\DataObject\ClassDefinition\Layout\Panel::__set_state(array(
             'name' => 'Centric',
             'type' => NULL,
             'region' => NULL,
             'title' => 'Centric',
             'width' => '',
             'height' => '',
             'collapsible' => false,
             'collapsed' => false,
             'bodyStyle' => '',
             'datatype' => 'layout',
             'children' => 
            array (
              0 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                 'name' => 'height',
                 'title' => 'Height',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => true,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'defaultValue' => NULL,
                 'columnLength' => 190,
                 'regex' => '',
                 'regexFlags' => 
                array (
                ),
                 'unique' => false,
                 'showCharCount' => false,
                 'width' => '',
                 'defaultValueGenerator' => '',
              )),
              1 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                 'name' => 'length',
                 'title' => 'Length',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => true,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'defaultValue' => NULL,
                 'columnLength' => 190,
                 'regex' => '',
                 'regexFlags' => 
                array (
                ),
                 'unique' => false,
                 'showCharCount' => false,
                 'width' => '',
                 'defaultValueGenerator' => '',
              )),
              2 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                 'name' => 'dimensionUomName',
                 'title' => 'Dimension Uom Name',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => true,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'options' => 
                array (
                  0 => 
                  array (
                    'key' => 'Inch',
                    'value' => 'Inch',
                  ),
                ),
                 'defaultValue' => '',
                 'optionsProviderClass' => '',
                 'optionsProviderData' => '',
                 'columnLength' => 190,
                 'dynamicOptions' => false,
                 'defaultValueGenerator' => '',
                 'width' => '',
              )),
              3 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                 'name' => 'weightUom',
                 'title' => 'Weight UOM',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => true,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'options' => 
                array (
                  0 => 
                  array (
                    'key' => '5',
                    'value' => '5',
                  ),
                  1 => 
                  array (
                    'key' => '6',
                    'value' => '6',
                  ),
                ),
                 'defaultValue' => '',
                 'optionsProviderClass' => '',
                 'optionsProviderData' => '',
                 'columnLength' => 190,
                 'dynamicOptions' => false,
                 'defaultValueGenerator' => '',
                 'width' => '',
              )),
              4 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                 'name' => 'upc',
                 'title' => 'UPC',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => true,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'defaultValue' => NULL,
                 'columnLength' => 190,
                 'regex' => '',
                 'regexFlags' => 
                array (
                ),
                 'unique' => false,
                 'showCharCount' => false,
                 'width' => '',
                 'defaultValueGenerator' => '',
              )),
              5 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                 'name' => 'division',
                 'title' => 'Division',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'options' => 
                array (
                  0 => 
                  array (
                    'key' => 'KIDS',
                    'value' => 'KIDS',
                  ),
                  1 => 
                  array (
                    'key' => 'MEN',
                    'value' => 'MEN',
                  ),
                  2 => 
                  array (
                    'key' => 'OTHER',
                    'value' => 'OTHER',
                  ),
                  3 => 
                  array (
                    'key' => 'WOMEN',
                    'value' => 'WOMEN',
                  ),
                ),
                 'defaultValue' => '',
                 'optionsProviderClass' => '',
                 'optionsProviderData' => '',
                 'columnLength' => 190,
                 'dynamicOptions' => false,
                 'defaultValueGenerator' => '',
                 'width' => '',
              )),
              6 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                 'name' => 'productProperty',
                 'title' => 'Property',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => true,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'options' => 
                array (
                  0 => 
                  array (
                    'key' => 'SUGARPLUM PIXIE',
                    'value' => 'SUGARPLUM PIXIE',
                  ),
                  1 => 
                  array (
                    'key' => 'PRINCESSES',
                    'value' => 'PRINCESSES',
                  ),
                  2 => 
                  array (
                    'key' => 'PARISIAN SLEIGH',
                    'value' => 'PARISIAN SLEIGH',
                  ),
                  3 => 
                  array (
                    'key' => 'PAPA NOEL',
                    'value' => 'PAPA NOEL',
                  ),
                  4 => 
                  array (
                    'key' => 'PAC-MAN',
                    'value' => 'PAC-MAN',
                  ),
                  5 => 
                  array (
                    'key' => 'MONSTER TRUCKS',
                    'value' => 'MONSTER TRUCKS',
                  ),
                  6 => 
                  array (
                    'key' => 'MINNIE',
                    'value' => 'MINNIE',
                  ),
                  7 => 
                  array (
                    'key' => 'MICKEY',
                    'value' => 'MICKEY',
                  ),
                  8 => 
                  array (
                    'key' => 'MARKIE MAGIC',
                    'value' => 'MARKIE MAGIC',
                  ),
                  9 => 
                  array (
                    'key' => 'LITTLE MERMAID',
                    'value' => 'LITTLE MERMAID',
                  ),
                  10 => 
                  array (
                    'key' => 'LISA FRANK LOGO',
                    'value' => 'LISA FRANK LOGO',
                  ),
                  11 => 
                  array (
                    'key' => 'HUNTER',
                    'value' => 'HUNTER',
                  ),
                  12 => 
                  array (
                    'key' => 'HOT WHEELS',
                    'value' => 'HOT WHEELS',
                  ),
                  13 => 
                  array (
                    'key' => 'HELLO KITTY',
                    'value' => 'HELLO KITTY',
                  ),
                  14 => 
                  array (
                    'key' => 'GLITTERVILLE',
                    'value' => 'GLITTERVILLE',
                  ),
                  15 => 
                  array (
                    'key' => 'FISHER PRICE',
                    'value' => 'FISHER PRICE',
                  ),
                  16 => 
                  array (
                    'key' => 'FAO SCHWARZ',
                    'value' => 'FAO SCHWARZ',
                  ),
                  17 => 
                  array (
                    'key' => 'DOLPHINS',
                    'value' => 'DOLPHINS',
                  ),
                  18 => 
                  array (
                    'key' => 'COLONEL CUPCAKE',
                    'value' => 'COLONEL CUPCAKE',
                  ),
                  19 => 
                  array (
                    'key' => 'BARBIE',
                    'value' => 'BARBIE',
                  ),
                  20 => 
                  array (
                    'key' => 'ALICE & OLIVIA',
                    'value' => 'ALICE & OLIVIA',
                  ),
                ),
                 'defaultValue' => '',
                 'optionsProviderClass' => '',
                 'optionsProviderData' => '',
                 'columnLength' => 190,
                 'dynamicOptions' => false,
                 'defaultValueGenerator' => '',
                 'width' => '',
              )),
              7 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                 'name' => 'costPrice',
                 'title' => 'Cost Price',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => true,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'defaultValue' => NULL,
                 'columnLength' => 190,
                 'regex' => '',
                 'regexFlags' => 
                array (
                ),
                 'unique' => false,
                 'showCharCount' => false,
                 'width' => '',
                 'defaultValueGenerator' => '',
              )),
              8 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Date::__set_state(array(
                 'name' => 'shipDate',
                 'title' => 'Ship Date',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => true,
                 'blockedVarsForExport' => 
                array (
                ),
                 'defaultValue' => NULL,
                 'useCurrentDate' => false,
                 'columnType' => 'bigint(20)',
                 'defaultValueGenerator' => '',
              )),
              9 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                 'name' => 'materialFamily',
                 'title' => 'Material Family',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'options' => 
                array (
                  0 => 
                  array (
                    'key' => 'BAMBOO COTTON',
                    'value' => 'BAMBOO COTTON',
                  ),
                  1 => 
                  array (
                    'key' => 'BAMBOO JERSEY',
                    'value' => 'BAMBOO JERSEY',
                  ),
                  2 => 
                  array (
                    'key' => 'BAMBOO RIB',
                    'value' => 'BAMBOO RIB',
                  ),
                  3 => 
                  array (
                    'key' => 'BAMBOO WAFFLE',
                    'value' => 'BAMBOO WAFFLE',
                  ),
                  4 => 
                  array (
                    'key' => 'CANVAS',
                    'value' => 'CANVAS',
                  ),
                  5 => 
                  array (
                    'key' => 'COTTON FRENCH TERRY',
                    'value' => 'COTTON FRENCH TERRY',
                  ),
                  6 => 
                  array (
                    'key' => 'COTTON POPLIN',
                    'value' => 'COTTON POPLIN',
                  ),
                  7 => 
                  array (
                    'key' => 'COTTON SPANDEX',
                    'value' => 'COTTON SPANDEX',
                  ),
                  8 => 
                  array (
                    'key' => 'JACQUARD',
                    'value' => 'JACQUARD',
                  ),
                  9 => 
                  array (
                    'key' => 'MUSLIN',
                    'value' => 'MUSLIN',
                  ),
                  10 => 
                  array (
                    'key' => 'NYLON',
                    'value' => 'NYLON',
                  ),
                  11 => 
                  array (
                    'key' => 'OTHER',
                    'value' => 'OTHER',
                  ),
                  12 => 
                  array (
                    'key' => 'PAPER',
                    'value' => 'PAPER',
                  ),
                  13 => 
                  array (
                    'key' => 'PLUSH',
                    'value' => 'PLUSH',
                  ),
                  14 => 
                  array (
                    'key' => 'POINTELLE',
                    'value' => 'POINTELLE',
                  ),
                  15 => 
                  array (
                    'key' => 'POLYESTER',
                    'value' => 'POLYESTER',
                  ),
                  16 => 
                  array (
                    'key' => 'POLYESTER SPANDEX',
                    'value' => 'POLYESTER SPANDEX',
                  ),
                  17 => 
                  array (
                    'key' => 'RAYON CHALLIS',
                    'value' => 'RAYON CHALLIS',
                  ),
                  18 => 
                  array (
                    'key' => 'SEASONAL FASHION',
                    'value' => 'SEASONAL FASHION',
                  ),
                  19 => 
                  array (
                    'key' => 'SWIM JERSEY',
                    'value' => 'SWIM JERSEY',
                  ),
                  20 => 
                  array (
                    'key' => 'TOWEL TERRY',
                    'value' => 'TOWEL TERRY',
                  ),
                ),
                 'defaultValue' => '',
                 'optionsProviderClass' => '',
                 'optionsProviderData' => '',
                 'columnLength' => 190,
                 'dynamicOptions' => false,
                 'defaultValueGenerator' => '',
                 'width' => '',
              )),
              10 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                 'name' => 'supplierName',
                 'title' => 'Supplier Name',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => true,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'options' => 
                array (
                  0 => 
                  array (
                    'key' => '{"Butterflyinflower Merchandise Firm "}',
                    'value' => '{"Butterflyinflower Merchandise Firm "}',
                  ),
                  1 => 
                  array (
                    'key' => '{"Guangzhou Goodmade Garments Accessories Co.,Limited"}',
                    'value' => '{"Guangzhou Goodmade Garments Accessories Co.,Limited"}',
                  ),
                  2 => 
                  array (
                    'key' => '{"High Fashion Garments International Co. Ltd.","Kdtex Co. Ltd"}',
                    'value' => '{"High Fashion Garments International Co. Ltd.","Kdtex Co. Ltd"}',
                  ),
                  3 => 
                  array (
                    'key' => '{"High Fashion Garments International Co. Ltd."}',
                    'value' => '{"High Fashion Garments International Co. Ltd."}',
                  ),
                  4 => 
                  array (
                    'key' => '{"JT Lace"}',
                    'value' => '{"JT Lace"}',
                  ),
                  5 => 
                  array (
                    'key' => '{"K Skirt Ecommerce"}',
                    'value' => '{"K Skirt Ecommerce"}',
                  ),
                  6 => 
                  array (
                    'key' => '{"Kdtex Co. Ltd","National Building Corp."}',
                    'value' => '{"Kdtex Co. Ltd","National Building Corp."}',
                  ),
                  7 => 
                  array (
                    'key' => '{"Kdtex Co. Ltd"}',
                    'value' => '{"Kdtex Co. Ltd"}',
                  ),
                  8 => 
                  array (
                    'key' => '{"Kids Blanks by Zoe"}',
                    'value' => '{"Kids Blanks by Zoe"}',
                  ),
                  9 => 
                  array (
                    'key' => '{"Lovely Babies apparel co., Ltd."}',
                    'value' => '{"Lovely Babies apparel co., Ltd."}',
                  ),
                  10 => 
                  array (
                    'key' => '{"Meijei Label&Printing LIMITED"}',
                    'value' => '{"Meijei Label&Printing LIMITED"}',
                  ),
                  11 => 
                  array (
                    'key' => '{"National Building Corp."}',
                    'value' => '{"National Building Corp."}',
                  ),
                  12 => 
                  array (
                    'key' => '{"One Hundred 80 Degrees (Glitterville)"}',
                    'value' => '{"One Hundred 80 Degrees (Glitterville)"}',
                  ),
                  13 => 
                  array (
                    'key' => '{"Queen Baby Store"}',
                    'value' => '{"Queen Baby Store"}',
                  ),
                  14 => 
                  array (
                    'key' => '{"TOP UNION INTERNATIONAL ENTERPRISE LIMITED "}',
                    'value' => '{"TOP UNION INTERNATIONAL ENTERPRISE LIMITED "}',
                  ),
                  15 => 
                  array (
                    'key' => '{Everich}',
                    'value' => '{Everich}',
                  ),
                  16 => 
                  array (
                    'key' => '{NULL}',
                    'value' => '{NULL}',
                  ),
                ),
                 'defaultValue' => '',
                 'optionsProviderClass' => '',
                 'optionsProviderData' => '',
                 'columnLength' => 190,
                 'dynamicOptions' => false,
                 'defaultValueGenerator' => '',
                 'width' => '',
              )),
              11 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                 'name' => 'license',
                 'title' => 'License',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => true,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'options' => 
                array (
                  0 => 
                  array (
                    'key' => 'ALICE & OLIVIA',
                    'value' => 'ALICE & OLIVIA',
                  ),
                  1 => 
                  array (
                    'key' => 'BNEA',
                    'value' => 'BNEA',
                  ),
                  2 => 
                  array (
                    'key' => 'DISNEY',
                    'value' => 'DISNEY',
                  ),
                  3 => 
                  array (
                    'key' => 'FAO SCHWARZ',
                    'value' => 'FAO SCHWARZ',
                  ),
                  4 => 
                  array (
                    'key' => 'GLITTERVILLE',
                    'value' => 'GLITTERVILLE',
                  ),
                  5 => 
                  array (
                    'key' => 'LISA FRANK',
                    'value' => 'LISA FRANK',
                  ),
                  6 => 
                  array (
                    'key' => 'MATTEL',
                    'value' => 'MATTEL',
                  ),
                  7 => 
                  array (
                    'key' => 'POSH PEANUT',
                    'value' => 'POSH PEANUTPOSH PEANUT',
                  ),
                  8 => 
                  array (
                    'key' => 'SANRIO',
                    'value' => 'SANRIO',
                  ),
                ),
                 'defaultValue' => '',
                 'optionsProviderClass' => '',
                 'optionsProviderData' => '',
                 'columnLength' => 190,
                 'dynamicOptions' => false,
                 'defaultValueGenerator' => '',
                 'width' => '',
              )),
              12 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                 'name' => 'hsCode',
                 'title' => 'Hs Code',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => true,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'options' => 
                array (
                  0 => 
                  array (
                    'key' => '4201.00.6000',
                    'value' => '4201.00.6000',
                  ),
                  1 => 
                  array (
                    'key' => '4202.92.0807',
                    'value' => '4202.92.0807',
                  ),
                  2 => 
                  array (
                    'key' => '4202.92.3120',
                    'value' => '4202.92.3120',
                  ),
                  3 => 
                  array (
                    'key' => '4202.92.3131',
                    'value' => '4202.92.3131',
                  ),
                  4 => 
                  array (
                    'key' => '6102.30.2020',
                    'value' => '6102.30.2020',
                  ),
                  5 => 
                  array (
                    'key' => '6103.43.0000',
                    'value' => '6103.43.0000',
                  ),
                  6 => 
                  array (
                    'key' => '6104.43.2010',
                    'value' => '6104.43.2010',
                  ),
                  7 => 
                  array (
                    'key' => '6104.44.2020',
                    'value' => '6104.44.2020',
                  ),
                  8 => 
                  array (
                    'key' => '6104.59.1060',
                    'value' => '6104.59.1060',
                  ),
                  9 => 
                  array (
                    'key' => '6104.63.1030',
                    'value' => '6104.63.1030',
                  ),
                  10 => 
                  array (
                    'key' => '6104.69.2030',
                    'value' => '6104.69.2030',
                  ),
                  11 => 
                  array (
                    'key' => '6107.12.0020',
                    'value' => '6107.12.0020',
                  ),
                  12 => 
                  array (
                    'key' => '6107.22.0000',
                    'value' => '6107.22.0000',
                  ),
                  13 => 
                  array (
                    'key' => '6107.22.0010',
                    'value' => '6107.22.0010',
                  ),
                  14 => 
                  array (
                    'key' => '6108.32.0000',
                    'value' => '6108.32.0000',
                  ),
                  15 => 
                  array (
                    'key' => '6108.32.0010',
                    'value' => '6108.32.0010',
                  ),
                  16 => 
                  array (
                    'key' => '6108.32.0025',
                    'value' => '6108.32.0025',
                  ),
                  17 => 
                  array (
                    'key' => '6108.92.0000',
                    'value' => '6108.92.0000',
                  ),
                  18 => 
                  array (
                    'key' => '6108.92.0025',
                    'value' => '6108.92.0025',
                  ),
                  19 => 
                  array (
                    'key' => '6108.92.0030',
                    'value' => '6108.92.0030',
                  ),
                  20 => 
                  array (
                    'key' => '6108.99.9000',
                    'value' => '6108.99.9000',
                  ),
                  21 => 
                  array (
                    'key' => '6109.90.1009',
                    'value' => '6109.90.1009',
                  ),
                  22 => 
                  array (
                    'key' => '6109.90.1060',
                    'value' => '6109.90.1060',
                  ),
                  23 => 
                  array (
                    'key' => '6110.30.3020',
                    'value' => '6110.30.3020',
                  ),
                  24 => 
                  array (
                    'key' => '6110.30.3059',
                    'value' => '6110.30.3059',
                  ),
                  25 => 
                  array (
                    'key' => '6110.90.9090',
                    'value' => '6110.90.9090',
                  ),
                  26 => 
                  array (
                    'key' => '6111.20.0000',
                    'value' => '6111.20.0000',
                  ),
                  27 => 
                  array (
                    'key' => '6111.20.6010',
                    'value' => '6111.20.6010',
                  ),
                  28 => 
                  array (
                    'key' => '6111.20.6070',
                    'value' => '6111.20.6070',
                  ),
                  29 => 
                  array (
                    'key' => '"6111.20.6070 "',
                    'value' => '"6111.20.6070 "',
                  ),
                  30 => 
                  array (
                    'key' => '6111.30.0000',
                    'value' => '6111.30.0000',
                  ),
                  31 => 
                  array (
                    'key' => '6111.30.5010',
                    'value' => '6111.30.5010',
                  ),
                  32 => 
                  array (
                    'key' => '6111.30.5070',
                    'value' => '6111.30.5070',
                  ),
                  33 => 
                  array (
                    'key' => '6111.90.1000',
                    'value' => '6111.90.1000',
                  ),
                  34 => 
                  array (
                    'key' => '6111.90.5010',
                    'value' => '6111.90.5010',
                  ),
                  35 => 
                  array (
                    'key' => '6111.90.5020',
                    'value' => '6111.90.5020',
                  ),
                  36 => 
                  array (
                    'key' => '6111.90.5070',
                    'value' => '6111.90.5070',
                  ),
                  37 => 
                  array (
                    'key' => '6111.90.9000',
                    'value' => '6111.90.9000',
                  ),
                  38 => 
                  array (
                    'key' => '6112.31.0000',
                    'value' => '6112.31.0000',
                  ),
                  39 => 
                  array (
                    'key' => '6112.41.0000',
                    'value' => '6112.41.0000',
                  ),
                  40 => 
                  array (
                    'key' => '6112.41.0010',
                    'value' => '6112.41.0010',
                  ),
                  41 => 
                  array (
                    'key' => '6112.41.0020',
                    'value' => '6112.41.0020',
                  ),
                  42 => 
                  array (
                    'key' => '6114.30.3030',
                    'value' => '6114.30.3030',
                  ),
                  43 => 
                  array (
                    'key' => '6114.90.9040',
                    'value' => '6114.90.9040',
                  ),
                  44 => 
                  array (
                    'key' => '6208.91.1000',
                    'value' => '6208.91.1000',
                  ),
                  45 => 
                  array (
                    'key' => '6208.92.0020',
                    'value' => '6208.92.0020',
                  ),
                  46 => 
                  array (
                    'key' => '6211.11.1010',
                    'value' => '6211.11.1010',
                  ),
                  47 => 
                  array (
                    'key' => '6211.11.1020',
                    'value' => '6211.11.1020',
                  ),
                  48 => 
                  array (
                    'key' => '6301.40.0020',
                    'value' => '6301.40.0020',
                  ),
                  49 => 
                  array (
                    'key' => '6301.90.0010',
                    'value' => '6301.90.0010',
                  ),
                  50 => 
                  array (
                    'key' => '6302.10.0020',
                    'value' => '6302.10.0020',
                  ),
                  51 => 
                  array (
                    'key' => '6302.60.0020',
                    'value' => '6302.60.0020',
                  ),
                  52 => 
                  array (
                    'key' => '6304.91.0170',
                    'value' => '6304.91.0170',
                  ),
                  53 => 
                  array (
                    'key' => '6505.00.1515',
                    'value' => '6505.00.1515',
                  ),
                  54 => 
                  array (
                    'key' => '6505.00.6030',
                    'value' => '6505.00.6030',
                  ),
                ),
                 'defaultValue' => '',
                 'optionsProviderClass' => '',
                 'optionsProviderData' => '',
                 'columnLength' => 190,
                 'dynamicOptions' => false,
                 'defaultValueGenerator' => '',
                 'width' => '',
              )),
              13 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                 'name' => 'lifecycle',
                 'title' => 'Lifecycle',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => 0,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'options' => 
                array (
                  0 => 
                  array (
                    'key' => 'ACTIVE',
                    'value' => 'ACTIVE',
                  ),
                  1 => 
                  array (
                    'key' => 'CLOSE-OUT',
                    'value' => 'CLOSE-OUT',
                  ),
                  2 => 
                  array (
                    'key' => 'DISCONTINUED',
                    'value' => 'DISCONTINUED',
                  ),
                  3 => 
                  array (
                    'key' => 'INACTIVE',
                    'value' => 'INACTIVE',
                  ),
                ),
                 'defaultValue' => '',
                 'optionsProviderClass' => '',
                 'optionsProviderData' => '',
                 'columnLength' => 190,
                 'dynamicOptions' => false,
                 'defaultValueGenerator' => '',
                 'width' => '',
              )),
            ),
             'locked' => false,
             'blockedVarsForExport' => 
            array (
            ),
             'fieldtype' => 'panel',
             'layout' => NULL,
             'border' => false,
             'icon' => '',
             'labelWidth' => 100,
             'labelAlign' => 'left',
          )),
        ),
         'locked' => false,
         'blockedVarsForExport' => 
        array (
        ),
         'fieldtype' => 'tabpanel',
         'border' => false,
         'tabPosition' => 'top',
      )),
    ),
     'locked' => false,
     'blockedVarsForExport' => 
    array (
    ),
     'fieldtype' => 'panel',
     'layout' => NULL,
     'border' => false,
     'icon' => NULL,
     'labelWidth' => 100,
     'labelAlign' => 'left',
  )),
   'icon' => '',
   'group' => '',
   'showAppLoggerTab' => false,
   'linkGeneratorReference' => '',
   'previewGeneratorReference' => '',
   'compositeIndices' => 
  array (
  ),
   'showFieldLookup' => false,
   'propertyVisibility' => 
  array (
    'grid' => 
    array (
      'id' => true,
      'key' => false,
      'path' => true,
      'published' => true,
      'modificationDate' => true,
      'creationDate' => true,
    ),
    'search' => 
    array (
      'id' => true,
      'key' => false,
      'path' => true,
      'published' => true,
      'modificationDate' => true,
      'creationDate' => true,
    ),
  ),
   'enableGridLocking' => false,
   'deletedDataComponents' => 
  array (
  ),
   'blockedVarsForExport' => 
  array (
  ),
   'fieldDefinitionsCache' => 
  array (
  ),
   'activeDispatchingEvents' => 
  array (
  ),
));
