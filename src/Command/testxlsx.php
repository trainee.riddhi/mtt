<?php

namespace App\Command;

use Pimcore\Console\AbstractCommand;
use Pimcore\Model\DataObject;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;

class testxlsx extends AbstractCommand
{
    protected function configure()
    {
        $this->setName("twizzle:xlsxExport:export")
            ->setDescription("Exported AssetMetaDataExport data")
            ->addOption(
                'monitoring-item-id',
                null,
                InputOption::VALUE_OPTIONAL,
                'Contains the monitoring item if executed via the Pimcore backend'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $fileName = 'Sample100_' . date("d-m-Y-H-i-s") . '.xlsx';
        $filePath = PIMCORE_WEB_ROOT . '/var/assets/xlsxTestData';

        if (!file_exists($filePath)) {
            mkdir($filePath, 0777, true);
        }

        $targetFile = $filePath . '/' . $fileName;

        // Create the XLSX spreadsheet
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        // Add header row
        $data = [
            'serialNumber', 'companyName', 'employeeMarkme', 'description', 'Leave', 'image', 'brand'
        ];
        $sheet->fromArray($data, NULL, "A1");

        $entries = new DataObject\Car\Listing();
        $carList = $entries->load();

        $row = 2; // Start writing data from row 2 (below the header)
        foreach ($carList as $carDataObject) {
            if ($carDataObject->getImage()) {
                $imagePath = $carDataObject->getImage()->getFilename();
            } else {
                $imagePath = "";
            }

            $brandArr = [];
            $brand = $carDataObject->getBrand();
            foreach ($brand as $brands) {
                $brandArr[] = $brands->getName();
            }

            // Add data to the worksheet
            $data = [
                $carDataObject->getKey(), $carDataObject->getCompanyName(),
                $carDataObject->getEmployeeMarkme(), $carDataObject->getDescription(),
                $carDataObject->getLeave(), $imagePath, implode(',', $brandArr)
            ];
            $sheet->fromArray($data, NULL, "A" . $row);

            $row++;
        }

        // Set the format for numeric cells
        $sheet_dimension = $sheet->calculateWorksheetDimension();
        $sheet->getStyle($sheet_dimension)->getNumberFormat()->setFormatCode('#');

        // Remove the unused default worksheet
        $spreadsheet->removeSheetByIndex(0);

        // Save the spreadsheet to the target file
        $writer = IOFactory::createWriter($spreadsheet, "Xlsx");
        $writer->save($targetFile);

        $output->writeln('Data exported to XLSX successfully.');

        // Save the XLSX file as an asset
        $exportedAssetMetaData = new \Pimcore\Model\Asset();
        $exportedAssetMetaData->setFilename($fileName);
        $dataFolderAsset = \Pimcore\Model\Asset\Service::createFolderByPath('/xlsxTestData');
        $exportedAssetMetaData->setParent($dataFolderAsset);
        $exportedAssetMetaData->setData(file_get_contents($targetFile)); // Set the asset data from the XLSX file
        $exportedAssetMetaData->save();

        return 0;
    }
}