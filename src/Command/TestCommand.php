<?php

namespace App\Command;

use Pimcore\Bundle\ApplicationLoggerBundle\ApplicationLogger;
use Pimcore\Bundle\ApplicationLoggerBundle\FileObject;
use Pimcore\Console\AbstractCommand;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Pimcore\Model\DataObject;
use PhpOffice\PhpSpreadsheet\IOFactory;

use Exception;

class TestCommand extends AbstractCommand
{
    use \Elements\Bundle\ProcessManagerBundle\ExecutionTrait;
    // const DATA_OBJECT_FOLDER_NAME = "car";
    protected $opiLogger;

    /**
     * @var MonitoringItem
     */
    protected $monitoringItem;

    public function __construct(ApplicationLogger $opiLogger)
    {
        parent::__construct();
        $this->opiLogger = $opiLogger;
    }

    protected function configure()
    {
        $this->setName('Import:opi:import')
        ->setDescription('Using this command you can import the opi data.')
        ->addOption(
            'monitoring-item-id',
            null,
            InputOption::VALUE_OPTIONAL,
            'Contains the monitoring item if executed via the Pimcore backend'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {

            $this->opiLogger->info("PROCESS_START :: To import opi data");

            // $myObject   = DataObject::getById($fileObject->);

            $filePath = '/importerImages/Sample100.csv';
            $asset = \Pimcore\Model\Asset::getByPath($filePath);

            $sourceFile = PIMCORE_WEB_ROOT . '/var/assets' . $filePath;

            if (!empty($asset)) {
                $fileObject = new FileObject('some interesting data');
                $inputFileType = IOFactory::identify($sourceFile);

                $reader = IOFactory::createReader($inputFileType);
                $spreadsheet = $reader->load($sourceFile);
                $workSheets = $spreadsheet->getSheetNames();
                $this->opiLogger->info("Reading '" . $workSheets[0] . "' sheet");
                $firstSheet = $spreadsheet->getSheet(0);
                $opiSheetData = $firstSheet->toArray();
                $this->opidata($opiSheetData);

                $this->opiLogger->info("PROCESS_END :: opi data completely imported!");
            } else {
                $this->opiLogger->info("Data file not found to import!");
            }
        } catch (\Exception $e) {
            $this->opiLogger->error('my error message', [
                'fileObject'    => $fileObject,
                // 'relatedObject' => $myObject,
                'component'     => 'different component',
                'source'        => 'Stack trace or context-relevant information' // optional, if empty, gets automatically filled with class:method:line from where the log got executed
            ]);
        }
        return 1;
    }

    protected function opidata($opiSheetData = [])
    {
        try {
            //Unset header from excel sheet
            $header = $opiSheetData[0];
            unset($opiSheetData[0]);

            foreach ($opiSheetData as $opidata) {
                $opiObj = self::checkExistingKey($opidata[0]);

                if (empty($opiObj)) {
                    $opiObj = new DataObject\Car();
                    $opiObj->setKey($opidata[0]);
                }

                $opiObj->setParent(\Pimcore\Model\DataObject\Service::createFolderByPath("carList"));

                foreach ($header as $fieldName) {

                    // set simple fields
                    if ($fieldName == 'SerialNumber' || $fieldName == 'EmployeeMarkme' || $fieldName == 'Description' || $fieldName == 'Leave') {
                        $index = array_search($fieldName, $header);
                        if ($index !== false) {
                            $setterMethod = 'set' . $fieldName;
                            $opiObj->$setterMethod($opidata[$index]);
                        }
                    } elseif ($fieldName == 'Image') {

                        // set Image Fields
                        $index = array_search($fieldName, $header);
                        $newAsset = '';
                        if ($opidata[$index]) {
                            $newAsset = new \Pimcore\Model\Asset\Image();
                            $newAsset->setFilename($opidata[1] . time() . 'test.jpg');
                            $newAsset->setData(file_get_contents($opidata[$index]));
                            $newAsset->setParent(\Pimcore\Model\Asset::getByPath("/importerImages"));
                            $newAsset->save();

                            $thumbnail = $newAsset->getThumbnail('crop', false);
                            $newAssetThumbnail = new \Pimcore\Model\Asset\Image();
                            $newAssetThumbnail->setFilename($opidata[1] . time() . 'thumbnail.jpg');
                            $newAssetThumbnail->setData(file_get_contents($thumbnail->getLocalFile()));
                            $newAssetThumbnail->setParent(\Pimcore\Model\Asset::getByPath("/thumbnail"));
                            $newAssetThumbnail->save();
                        }
                        if ($newAsset) {
                            if ($index !== false) {
                                $setterMethod = 'set' . $fieldName;
                                $opiObj->$setterMethod($newAsset);
                            }
                            $opiObj->setImageThumbnail($newAssetThumbnail);
                        }
                    } elseif ($fieldName == 'Gallery') {

                        // Set Image Gallery Fields
                        $index = array_search($fieldName, $header);

                        $imgGallery = explode(",", $opidata[$index]);

                        $galleryData = [];

                        foreach ($imgGallery as $imgGalleries) {
                            if (!empty($imgGalleries)) {
                                $newImageGallery = new \Pimcore\Model\Asset\Image();
                                $newImageGallery->setFilename($opidata[1] . date("H-i-s") . 'gallery.jpg');
                                $newImageGallery->setData(file_get_contents($imgGalleries));
                                $newImageGallery->setParent(\Pimcore\Model\Asset::getByPath("/importerImages"));
                                $newImageGallery->save();
                                $galleryData[] = $newImageGallery;
                            }
                        }

                        $items = [];
                        foreach ($galleryData as $img) {
                            $advancedImage = new \Pimcore\Model\DataObject\Data\Hotspotimage();
                            $advancedImage->setImage($img);
                            $items[] = $advancedImage;
                        }
                        if ($index !== false) {
                            $setterMethod = 'set' . $fieldName;
                            $opiObj->$setterMethod(new \Pimcore\Model\DataObject\Data\ImageGallery($items));
                        }
                    } elseif ($fieldName == 'Brand') {

                        // Set Brand Relational Field
                        $index = array_search($fieldName, $header);
                        $relation = explode(",", $opidata[$index]);
                        $relationArr = [];
                        if ($opidata[$index]) {
                            foreach ($relation as $relations) {
                                $brandObj = new DataObject\Brand();
                                $brandObj->setKey($relations);
                                $brandObj->setParent(\Pimcore\Model\DataObject\Service::createFolderByPath("brandList"));
                                $brandObj->setName($relations);
                                $brandObj->setPublished(true);
                                $brandObj->save();

                                $category = DataObject\Brand::getByName($relations, true);

                                if ($category) {
                                    $relationArr[$category->getId()] = $category;
                                }
                            }
                        }
                        if ($relationArr) {
                            if ($index !== false) {
                                $setterMethod = 'set' . $fieldName;
                                $opiObj->$setterMethod($relationArr);
                            }
                        }
                    } else {

                        // Set Localize Field
                        $languages = \Pimcore\Tool::getValidLanguages();
                        foreach ($languages as $language) {
                            $index = array_search('CompanyName_' . $language, $header);
                            $opiObj->setCompanyName($opidata[$index], $language);
                        }
                    }
                }
                $opiObj->setPublished(true);
                $opiObj->save();
            }
        } catch (Exception $e) {
            p_r($e);
            $this->opiLogger->error('Error while importing taxonomies: ' . $e->getMessage());
        }
    }
    /**
     * For check existing opi
     * @param string $key
     *
     */
    public function checkExistingKey($key)
    {
        $list = new DataObject\Car\Listing();
        // $list->filterByKey($key);

        $list->setCondition("serialNumber LIKE :serialNumber", ["serialNumber" => "%$key%"]);
        $list->setLimit(1);
        $master = $list->load();
        return (!empty($master)) ? $master[0] : null;
    }
}
