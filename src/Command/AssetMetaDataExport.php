<?php

namespace App\Command;

use Pimcore\Console\AbstractCommand;
use Pimcore\Model\Asset;
use Pimcore\Model\DataObject;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Elements\Bundle\ProcessManagerBundle\Model\MonitoringItem;
use Monolog\Logger;

use function PHPUnit\Framework\isNull;

class AssetMetaDataExport extends AbstractCommand
{
    // use \Elements\Bundle\ProcessManagerBundle\ExecutionTrait;

    /**
     * @var LoggerInterface:
     */
    protected $logger;

    /**opi
     * @var MonitoringItem
     */
    protected $monitoringItem;

    public function configure()
    {
        $this->setName("twizzle:AssetMetaDataExport:export")
            ->setDescription("Exported AssetMetaDataExport data")
            ->addOption(
                'monitoring-item-id',
                null,
                InputOption::VALUE_OPTIONAL,
                'Contains the monitoring item if executed via the Pimcore backend'
            );
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $fileName = 'Sample100' . date("d-m-Y-H-i-s") . '.csv';
        $filePath = PIMCORE_WEB_ROOT . '/var/assets/carData';

        if (!file_exists($filePath)) {
            mkdir($filePath, 0777, true);
        }

        $targetFile = $filePath . '/' . $fileName;
        $file = fopen($targetFile, 'w');

        $data = [
            'serialNumber', 'companyName', 'employeeMarkme', 'description', 'Leave', 'image', 'brand'
        ];

        $entries = new DataObject\Car\Listing();
        $carList = $entries->load();

        fputcsv($file, $data, ';');

        foreach ($carList as $carDataObject) {
            if ($carDataObject->getImage())
                $imagePath = $carDataObject->getImage()->getFilename();
            else{
                $imagePath = "";
            }

            $brand = $carDataObject->getBrand();
            $brandArr = [];
            // if ($brand)
                foreach ($brand as $brands) {
                    $brandArr[] = $brands->getName();
                }
            // if (!empty($brandArr))
                $data = [
                    $carDataObject->getKey(),$carDataObject->getCompanyName(), $carDataObject->getEmployeeMarkme(), $carDataObject->getDescription(), $carDataObject->getLeave(), $imagePath, implode(',', $brandArr)
                ];
                p_r($data);
            fputcsv($file, $data);
        }
        
        $exportedAssetMetaData = new \Pimcore\Model\Asset();
        $exportedAssetMetaData->setFilename($fileName);
        $dataFolderAsset = \Pimcore\Model\Asset\Service::createFolderByPath('/carData');
        $exportedAssetMetaData->setParent($dataFolderAsset);
        $exportedAssetMetaData->save();

        fclose($file);

        return 0;
    }
}