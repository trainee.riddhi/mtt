<?php

namespace App\Command;

use Codeception\Lib\Di;
use Pimcore\Bundle\ApplicationLoggerBundle\ApplicationLogger;
use Pimcore\Bundle\ApplicationLoggerBundle\FileObject;
use Pimcore\Console\AbstractCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Pimcore\Model\DataObject;

use Exception;

class JSONData extends AbstractCommand
{
    // const DATA_OBJECT_FOLDER_NAME = "car";
    protected $opiLogger;

    public function __construct(ApplicationLogger $opiLogger)
    {
        parent::__construct();
        $this->opiLogger = $opiLogger;
    }

    protected function configure()
    {
        $this->setName('Import:json:import')->setDescription('Using this command you can import the opi data.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {

            $this->opiLogger->info("PROCESS_START :: To import opi data");

            // $myObject   = DataObject::getById($fileObject->);

            $filePath = '/importerImages/Sample100.csv';
            $asset = \Pimcore\Model\Asset::getByPath($filePath);

            $sourceFile = PIMCORE_WEB_ROOT . '/var/assets' . $filePath;

            if (!empty($asset)) {
                $fileObject = new FileObject('some interesting data');
                $this->opidata();

                $this->opiLogger->info("PROCESS_END :: opi data completely imported!");
            } else {
                $this->opiLogger->info("Data file not found to import!");
            }
        } catch (\Exception $e) {
            $this->opiLogger->error('my error message', [
                'fileObject'    => $fileObject,
                // 'relatedObject' => $myObject,
                'component'     => 'different component',
                'source'        => 'Stack trace or context-relevant information' // optional, if empty, gets automatically filled with class:method:line from where the log got executed
            ]);
        }
        return 1;
    }

    protected function opidata()
    {
        try {

            $json = '{
                "accountCategory": "Parent product category  / ONE PIECE",
                "templateName": "Midnight Rose - Footie Ruffled Zippered One Piece",
                "active": true,
                "brand": "POSH PEANUT",
                "productCode": "PP-OP007-MDR-0-3M",
                "costPrice": {
                  "__class__": "Decimal",
                  "decimal": "7.60"
                },
                "priceMethod": "average",
                "dimensionUomName": "Inch",
                "height": 0.5,
                "length": 9.25,
                "listPrice": {
                  "__class__": "Decimal",
                  "decimal": "40.00"
                },
                "upc": "196137047343",
                "variantName": "Midnight Rose - Footie Ruffled Zippered One Piece - 0-3 months",
                "width": 7.6,
                "weight": 4.3,
                "weightUom": 6,
                "weightUomName": "Ounce",
                "hsCode": "6111.30.0000",
                "customsDescription": "One Piece/Infants` Clothing",
                "productType": "goods",
                "year": null,
                "supplierName": "Kdtex Co. Ltd",
                "productCodeOther": "MDRPP-OP007",
                "attributes": {
                  "styleCode": "PP-OP007",
                  "colorCode": "MDR",
                  "sizeCode": "0-3M",
                  "size": "0-3 Months",
                  "sizeCodeName": null,
                  "printFamily": "FLORALS",
                  "colorFamily": "BLACK",
                  "nrfColorName": "OXFORD",
                  "nrfColorCode": "3",
                  "subCategory": "FOOTIES",
                  "classification": "N/A",
                  "season": "Holiday 2021",
                  "gender": "Girl",
                  "ageRange": "INFANT",
                  "materials": "Bamboo/Spandex(95/5)",
                  "slvLength": "LONG",
                  "bottomLength": "FOOTIE",
                  "closure": "ZIP",
                  "department": "Apparel",
                  "nuCustomerGroups": "All",
                  "costPriceMethod": null,
                  "channel": "MULTICHANNEL",
                  "division": "KIDS",
                  "productProperty": "ALICE & OLIVIA",
                  "materialFamily": "BAMBOO JERSEY",
                  "license": "ALICE & OLIVIA",
                  "lifecycle": "DISCONTINUED",
                  "collection": "MIDNIGHT ROSE",
                  "currentPrice": "40",
                  "styleDescription": "FOOTIE RUFFLED ZIPPERED ONE PIECE",
                  "launchDate": "2021-10-11",
                  "shipDate": "2021-10-25",
                  "datetimeAdded": null,
                  "preBook": "False",
                  "priceMethod": "REG"
                }
              }';

            $jsonArray = json_decode($json, true);
    
            $opiObj = self::checkExistingKey($jsonArray['brand']);

            if (empty($opiObj)) {
                $opiObj = new DataObject\Product();
                $opiObj->setKey($jsonArray['brand']);
            }

            $opiObj->setParent(\Pimcore\Model\DataObject\Service::createFolderByPath("product"));
            $opiObj->setAccountCategory($jsonArray['accountCategory']);
            $opiObj->setTemplateName($jsonArray['templateName']);
            $opiObj->setActive($jsonArray['active']);


            // set brand
            $brandObj = new DataObject\Brand();
            $brandObj->setKey($jsonArray['brand']);
            $brandObj->setParent(\Pimcore\Model\DataObject\Service::createFolderByPath("brandList"));
            $brandObj->setBrand($jsonArray['brand']);
            $brandObj->setPublished(true);
            // $brandObj->save();

            $brand = DataObject\Brand::getByPath('/brandList/POSH PEANUT');
            $opiObj->setBrand($brand);

            $opiObj->setProductCode($jsonArray['productCode']);
            $opiObj->setCostPrice($jsonArray['costPrice']['decimal']);
            $opiObj->setDimensionUomName($jsonArray['dimensionUomName']);
            $opiObj->setHeight($jsonArray['height']);
            $opiObj->setLength($jsonArray['length']);

            // set list price
            $listPriceArr = [];
            $listPriceObj = new DataObject\ListPrice();
            $listPriceObj->setKey($jsonArray['listPrice']['decimal']);
            $listPriceObj->setParent(\Pimcore\Model\DataObject\Service::createFolderByPath("listPrice"));
            $listPriceObj->setListPrice($jsonArray['listPrice']['decimal']);
            $listPriceObj->setPublished(true);
            // $listPriceObj->save();

            $category = DataObject\ListPrice::getByPath('/listPrice/40.00');
            // p_r($category);
            if ($category) {
                $listPriceArr[$category->getId()] = $category;
            }
            $opiObj->setListPrice($listPriceArr);

            $opiObj->setUpc($jsonArray['upc']);
            $opiObj->setVariantName($jsonArray['variantName']);
            $opiObj->setWidth($jsonArray['width']);
            $opiObj->setWeight($jsonArray['weight']);
            $opiObj->setWeightUom($jsonArray['weightUom']);
            $opiObj->setHsCode($jsonArray['hsCode']);
            $opiObj->setCustomsDescription($jsonArray['customsDescription']);
            $opiObj->setProductType($jsonArray['productType']);
            $opiObj->setYear($jsonArray['year']);
            $opiObj->setSupplierName($jsonArray['supplierName']);
            $opiObj->setProductCodeOther($jsonArray['productCodeOther']);

            // set styleCode
            $styleCodeObj = new DataObject\StyleCode();
            $styleCodeObj->setKey($jsonArray['attributes']['styleCode']);
            $styleCodeObj->setParent(\Pimcore\Model\DataObject\Service::createFolderByPath("styleCode"));
            $styleCodeObj->setStyleCode($jsonArray['attributes']['styleCode']);
            $styleCodeObj->setPublished(true);
            // $styleCodeObj->save();

            $styleCode = DataObject\StyleCode::getByPath('/styleCode/PP-OP007');
            $opiObj->setStyleCode($styleCode);

            // set colorCode
            $colorCodeObj = new DataObject\ColorCode();
            $colorCodeObj->setKey($jsonArray['attributes']['colorCode']);
            $colorCodeObj->setParent(\Pimcore\Model\DataObject\Service::createFolderByPath("colorCode"));
            $colorCodeObj->setColorCode($jsonArray['attributes']['colorCode']);
            $colorCodeObj->setPublished(true);
            // $colorCodeObj->save();

            $colorCode = DataObject\ColorCode::getByPath('/colorCode/MDR');
            $opiObj->setColorCode($colorCode);

            // set sizeCode
            $sizeCodeObj = new DataObject\SizeCode();
            $sizeCodeObj->setKey($jsonArray['attributes']['sizeCode']);
            $sizeCodeObj->setParent(\Pimcore\Model\DataObject\Service::createFolderByPath("sizeCode"));
            $sizeCodeObj->setSizeCode($jsonArray['attributes']['sizeCode']);
            $sizeCodeObj->setPublished(true);
            // $sizeCodeObj->save();

            $sizeCode = DataObject\SizeCode::getByPath('/sizeCode/0-3M');
            $opiObj->setSizeCode($sizeCode);

            // set size
            $sizeObj = new DataObject\Size();
            $sizeObj->setKey($jsonArray['attributes']['size']);
            $sizeObj->setParent(\Pimcore\Model\DataObject\Service::createFolderByPath("size"));
            $sizeObj->setSize($jsonArray['attributes']['size']);
            $sizeObj->setPublished(true);
            // $sizeObj->save();

            $size = DataObject\Size::getByPath('/size/0-3 Months');
            $opiObj->setSize($size);

            // set printFamily
            $printFamilyObj = new DataObject\PrintFamily();
            $printFamilyObj->setKey($jsonArray['attributes']['printFamily']);
            $printFamilyObj->setParent(\Pimcore\Model\DataObject\Service::createFolderByPath("printFamily"));
            $printFamilyObj->setPrintFamily($jsonArray['attributes']['printFamily']);
            $printFamilyObj->setPublished(true);
            // $printFamilyObj->save();

            $printFamily = DataObject\PrintFamily::getByPath('/printFamily/FLORALS');
            $opiObj->setPrintFamily($printFamily);

            $opiObj->setColorFamily($jsonArray['attributes']['colorFamily']);
            $opiObj->setNrfColorName($jsonArray['attributes']['nrfColorName']);
            $opiObj->setNrfColorCode($jsonArray['attributes']['nrfColorCode']);
            $opiObj->setSubCategory($jsonArray['attributes']['subCategory']);
            $opiObj->setClassification($jsonArray['attributes']['classification']);
            $opiObj->setSeason($jsonArray['attributes']['season']);
            $opiObj->setGender($jsonArray['attributes']['gender']);
            $opiObj->setAgeRange($jsonArray['attributes']['ageRange']);
            $opiObj->setMaterials($jsonArray['attributes']['materials']);
            $opiObj->setSlvLength($jsonArray['attributes']['slvLength']);
            $opiObj->setBottomLength($jsonArray['attributes']['bottomLength']);
            $opiObj->setClosure($jsonArray['attributes']['closure']);
            $opiObj->setDepartment($jsonArray['attributes']['department']);
            $opiObj->setNuCustomerGroups($jsonArray['attributes']['nuCustomerGroups']);
            $opiObj->setCostPriceMethod($jsonArray['attributes']['costPriceMethod']);
            $opiObj->setChannel($jsonArray['attributes']['channel']);
            $opiObj->setDivision($jsonArray['attributes']['division']);
            $opiObj->setProductProperty($jsonArray['attributes']['productProperty']);
            $opiObj->setMaterialFamily($jsonArray['attributes']['materialFamily']);
            $opiObj->setLicense($jsonArray['attributes']['license']);
            $opiObj->setLifecycle($jsonArray['attributes']['lifecycle']);
            $opiObj->setCollection($jsonArray['attributes']['collection']);
            $opiObj->setCurrentPrice($jsonArray['attributes']['currentPrice']);
            $opiObj->setStyleDescription($jsonArray['attributes']['styleDescription']);
            $opiObj->setLaunchDate(\Carbon\Carbon::create($jsonArray['attributes']['launchDate']));
            $opiObj->setShipDate(\Carbon\Carbon::create($jsonArray['attributes']['shipDate']));
            $opiObj->setDatetimeAdded(\Carbon\Carbon::create($jsonArray['attributes']['datetimeAdded']));
            $opiObj->setPreBook($jsonArray['attributes']['preBook']);

            // set attributes price method
            $priceMethodObj = new DataObject\PriceMethod();
            $priceMethodObj->setKey($jsonArray['attributes']['priceMethod']);
            $priceMethodObj->setParent(\Pimcore\Model\DataObject\Service::createFolderByPath("priceMethod"));
            $priceMethodObj->setPriceMethod($jsonArray['attributes']['priceMethod']);
            $priceMethodObj->setPublished(true);
            // $priceMethodObj->save();
            $priceMethod = DataObject\PriceMethod::getByPath('/priceMethod/REG');
            $opiObj->setPriceMethod($priceMethod);

            $opiObj->setPublished(true);
            $opiObj->save();
        } catch (Exception $e) {
            p_r($e);
            $this->opiLogger->error('Error while importing taxonomies: ' . $e->getMessage());
        }
    }
    /**
     * For check existing opi
     * @param string $key
     *
     */
    public function checkExistingKey($key)
    {
        // $list = new DataObject\Product\Listing();
        // // $list->filterByKey($key);

        // $list->setCondition("brand LIKE :brand", ["brand" => "%$key%"]);
        // $list->setLimit(1);
        // $master = $list->load();
        // return (!empty($master)) ? $master[0] : null;
    }
}
