<?php

namespace App\Command;

use Pimcore\Console\AbstractCommand;
use Pimcore\Model\Asset;
use Pimcore\Model\DataObject;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Elements\Bundle\ProcessManagerBundle\Model\MonitoringItem;
use Monolog\Logger;


class ObjectUpdateCommand extends AbstractCommand
{
    use \Elements\Bundle\ProcessManagerBundle\ExecutionTrait;

    /**
     * @var LoggerInterface:
     */
    protected $logger;


    /**
     * @var MonitoringItem
     */
    protected $monitoringItem;

    public function configure()
    {
        $this->setName("Poshpeanut:ObjectUpdate")
            ->setDescription("Exported AssetMetaDataExport data")
            ->addOption(
                'monitoring-item-id',
                null,
                InputOption::VALUE_OPTIONAL,
                'Contains the monitoring item if executed via the Pimcore backend'
            );
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {


        $this->initProcessManager($input->getOption('monitoring-item-id'), ['autoCreate' => true]);
        $this->monitoringItem = $this->getMonitoringItem();
        $this->monitoringItem->setTotalSteps(1)->save();
        $this->logger = $this->monitoringItem->getLogger();
        $callbackSettings = $this->monitoringItem->getCallbackSettings();
        $puzzlePack = (isset($callbackSettings['puzzlePack'])) ? $callbackSettings['puzzlePack'] : null;

        $aspectRatio = (isset($callbackSettings['aspectRatio'])) ? $callbackSettings['aspectRatio'] : null;

        try {
            $fileName = 'pack_image_metadata_' . date("d-m-Y-H-i-s") . '.csv';
            $filePath = PIMCORE_WEB_ROOT . '/var/assets/Metadata File/New/Exported/Asset MetaData';

            if (!file_exists($filePath)) {
                mkdir($filePath, 0777, true);
            }

            $targetFile = $filePath . '/' . $fileName;
            $file = fopen($targetFile, 'w');

            $data = [
                "Pack ID", "Image file name", "Aspect Ratio", "Image Keywords",
                "Image thumbnail1", "Image thumbnail2", "Image Order"
            ];

            fputcsv($file, $data, ';');
            $puzzlePackId = $puzzlePack['id'];
            $puzzlePack =  DataObject\Car::getById($puzzlePackId);

            if ($puzzlePack->getWorkflowState() != 'approved') {
                $this->logger->critical('Approved pack not found');
                $this->monitoringItem
                    ->setCurrentStep(1)
                    ->setTotalSteps(1)
                    ->setCurrentWorkload(1)
                    ->setTotalWorkload(1)
                    ->setMessage(' Approved data not found', Logger::CRITICAL)
                    ->setStatus(MonitoringItem::STATUS_FAILED)->save();
                return 1;
            }

            $assetmetaData = $puzzlePack->getAssetMetaData();

            $totalRecords = count($assetmetaData);
            if ($totalRecords == 0) {
                $this->logger->critical('Asset metadata not found');
                $this->monitoringItem
                    ->setCurrentStep(1)
                    ->setTotalSteps(1)
                    ->setCurrentWorkload(1)
                    ->setTotalWorkload(1)
                    ->setMessage('Asset metadata not found', Logger::CRITICAL)
                    ->setStatus(MonitoringItem::STATUS_FAILED)->save();
                return 1;
            }
            $this->monitoringItem->getLogger()->info("Export process start");

            $thumbnailName = '';
            if ($aspectRatio == '2:1' or $aspectRatio == '3:2' or $aspectRatio == '4:3' or $aspectRatio == '5:4'  or $aspectRatio == '5:3') {
                $thumbnailName = 'HorizontalRatio2048';
            } elseif ($aspectRatio == '1:1') {
                $thumbnailName = 'SquareRatio2048';
            } elseif ($aspectRatio == '1:2' or $aspectRatio == '2:3' or $aspectRatio == '3:4' or $aspectRatio == '4:5') {
                $thumbnailName = 'VeritcalRatio2048';
            }

            $thumbnailDefault = 'TW-tmb';

            $assetOrder = 1;
            $flag = false;
            foreach ($assetmetaData as $metaDataObject) {

                if ($metaDataObject->getAspectRatioTest() == $aspectRatio) {
                    $flag = true;
                    $ftpThumbnailPath1 = '';
                    $ftpThumbnailPath2 = '';
                    $imagePath = '';

                    if ($metaDataObject->getImage()) {
                        $imagePath = $metaDataObject->getImage()->getFileName();
                        //For  getting default thumbnail
                        $thumbnail1 = $metaDataObject->getImage()->getThumbnail($thumbnailDefault);
                        if ($thumbnail1) {
                            $thumbnailFile = $thumbnail1->getLocalFile();
                            $ftpThumbnailPath1 = self::sendImageOnFtp($thumbnailFile, $thumbnailDefault);
                        }

                        //For  getting 2nd thumbnail
                        $thumbnail2 = $metaDataObject->getImage()->getThumbnail($thumbnailName);
                        if ($thumbnail2) {
                            $thumbnailFile = $thumbnail2->getLocalFile();
                            $ftpThumbnailPath2 = self::sendImageOnFtp($thumbnailFile, $thumbnailName);
                        }
                    }

                    $data = [
                        $puzzlePack->getId(), $imagePath,
                        $metaDataObject->getAspectRatioTest(), $metaDataObject->getKeywords(),
                        $ftpThumbnailPath1, $ftpThumbnailPath2, $assetOrder
                    ];
                    fputcsv($file, $data, ';');
                }

                $assetOrder++;
            }
            if ($flag) {
                $exportedAssetMetaData = new \Pimcore\Model\Asset();
                $exportedAssetMetaData->setFilename($fileName);
                $dataFolderAsset = \Pimcore\Model\Asset\Service::createFolderByPath('/Metadata File/New/Exported/Asset MetaData');
                $exportedAssetMetaData->setParent($dataFolderAsset);
                $exportedAssetMetaData->save();
            }
            fclose($file);

            $this->monitoringItem
                ->setCurrentWorkload(1)
                ->setTotalWorkload(1)
                ->setMessage('Export process finished')
                ->save();
            $this->monitoringItem->setMessage('Job finished')->setCompleted();
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
            $this->monitoringItem
                ->setCurrentStep(1)
                ->setTotalSteps(1)
                ->setCurrentWorkload(1)
                ->setTotalWorkload(1)
                ->setMessage('aborted', Logger::CRITICAL)
                ->setStatus(MonitoringItem::STATUS_FAILED)->save();
            return 1;
        }
        return 0;
    }

    /**
     *  Get Folder Child Function
     *
     * @param [type] $parentId
     *
     * @return array
     */
    public function sendImageOnFtp($tempThumbnailPath, $thumbnailName)
    {

        //Get detail of ftp credential from website setting
        $host = \Pimcore\Model\WebsiteSetting::getByName('HOST');
        $host = ($host && $host->getData()) ? $host->getData() : '';

        $user = \Pimcore\Model\WebsiteSetting::getByName('FTPUSER');
        $username = ($user && $user->getData()) ? $user->getData() : '';

        $password = \Pimcore\Model\WebsiteSetting::getByName('PASSWORD');
        $password = ($password && $password->getData()) ? $password->getData() : '';

        $port = \Pimcore\Model\WebsiteSetting::getByName('PORT');
        $port = ($port && $port->getData()) ? $port->getData() : '';

        $remote_file = \Pimcore\Model\WebsiteSetting::getByName('REMOTEDIRFORASSETMETADATA');
        $remote_file = ($remote_file && $remote_file->getData()) ? $remote_file->getData() : '';

        //Check connection and ssh2 exists
        // if (!function_exists("ssh2_connect"))
        //     die('Function ssh2_connect not found, you cannot use ssh2 here');

        // if (!$connection = ssh2_connect($host, $port))
        //     die('Unable to connect');

        // if (!ssh2_auth_password($connection, $username, $password))
        //     die('Unable to authenticate.');

        // if (!$sftp = ssh2_sftp($connection))
        //     die('Unable to create a stream.');

        // Initialize SFTP subsystem
        $sftp = '';
        // $sftp = ssh2_sftp($connection);

        $imageName = pathinfo($tempThumbnailPath, PATHINFO_FILENAME);
        $extension = pathinfo($tempThumbnailPath, PATHINFO_EXTENSION);

        $ftpImagePath = $imageName . '_' . $thumbnailName . '.' . $extension;
        $destinationPath = $remote_file . '/' . $ftpImagePath;

        // Upload the temporary file
        $stream = fopen("ssh2.sftp://{$sftp}/{$destinationPath}", 'w');
        if (!$stream) {
            die('Could not open remote file.');
        }

        $data = file_get_contents($tempThumbnailPath);
        if (fwrite($stream, $data) === false) {
            die('Could not send data from file.');
        }

        fclose($stream);

        // Remove the temporary file
        unlink($tempThumbnailPath);

        return $destinationPath;
    }
}
