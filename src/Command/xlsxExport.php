<?php

namespace App\Command;

use Pimcore\Console\AbstractCommand;
use Pimcore\Model\Asset;
use Pimcore\Model\DataObject;
use Symfony\Component\Console\Input\InputInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Elements\Bundle\ProcessManagerBundle\Model\MonitoringItem;
use Monolog\Logger;

use function PHPUnit\Framework\isNull;

class xlsxExport extends AbstractCommand
{
    // use \Elements\Bundle\ProcessManagerBundle\ExecutionTrait;

    /**
     * @var LoggerInterface:
     */
    protected $logger;

    /**opi
     * @var MonitoringItem
     */
    protected $monitoringItem;

    public function configure()
    {
        $this->setName("twizzle:xlsdExport:export")
            ->setDescription("Exported AssetMetaDataExport data")
            ->addOption(
                'monitoring-item-id',
                null,
                InputOption::VALUE_OPTIONAL,
                'Contains the monitoring item if executed via the Pimcore backend'
            );
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $fileName = 'Sample100' . date("d-m-Y-H-i-s") . '.xlsx';
        $filePath = PIMCORE_WEB_ROOT . '/var/assets/xlsxData';

        if (!file_exists($filePath)) {
            mkdir($filePath, 0777, true);
        }

        $targetFile = $filePath . '/' . $fileName;
        $file = fopen($targetFile, 'w');

        $spreadsheet = new Spreadsheet();

        // Create a new sheet
        $sheet = $spreadsheet->createSheet(0);

        $data = [
            'serialNumber', 'companyName', 'employeeMarkme', 'description', 'Leave', 'image', 'brand'
        ];

        $entries = new DataObject\Car\Listing();
        $carList = $entries->load();

        $sheet->fromArray($data, NULL, "A1");
        $row = 2;
        foreach ($carList as $carDataObject) {
            if ($carDataObject->getImage())
                $imagePath = $carDataObject->getImage()->getFilename();
            else {
                $imagePath = "";
            }

            $brand = $carDataObject->getBrand();
            $brandArr = [];
            // if ($brand)
            foreach ($brand as $brands) {
                $brandArr[] = $brands->getName();
            }
            // if (!empty($brandArr))
            $data = [
                $carDataObject->getKey(), $carDataObject->getCompanyName(), $carDataObject->getEmployeeMarkme(), $carDataObject->getDescription(), $carDataObject->getLeave(), $imagePath, implode(',', $brandArr)
            ];
            // Add data to the worksheet from the $data array starting at cell A1
            $sheet->fromArray($data, NULL, "A" . $row);

            $sheet_dimension = $sheet->calculateWorksheetDimension();
            $sheet->getStyle($sheet_dimension)->getNumberFormat();

            // Remove the second worksheet (index 1)
            // $spreadsheet->removeSheetByIndex(1);

            $writer = IOFactory::createWriter($spreadsheet, "Xlsx");

            // Save the spreadsheet to the target file  
            $writer->save($targetFile);

            $row++;
        }

        $exportedAssetMetaData = new \Pimcore\Model\Asset();
        $exportedAssetMetaData->setFilename($fileName);
        $dataFolderAsset = \Pimcore\Model\Asset\Service::createFolderByPath('/xlsxData');
        $exportedAssetMetaData->setParent($dataFolderAsset);
        $exportedAssetMetaData->save();

        fclose($file);

        return 0;
    }
}
