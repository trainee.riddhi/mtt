<?php
namespace App\EventListener;

use Pimcore\Event\BundleManager\PathsEvent;

class TestListener
{
    public function addJSFiles(PathsEvent $event): void
    {
        $event->setPaths(
            array_merge(
                $event->getPaths(),
                [
                    '/js/admin.js'
                ]
            )
        );
    }
}