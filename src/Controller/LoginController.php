<?php

namespace App\Controller;

use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use \Pimcore\Model\DataObject;
use Symfony\Component\HttpFoundation\Session\SessionInterface; 

class LoginController extends FrontendController
{
    /**
     * @Route("/loginform")
     * @param Request $request
     * @return Response
     */
    public function loginform(Request $request): Response
    {
        return $this->render('form/login.html.twig');
    }

    /**
     * @Route("/login")
     * @param Request $request
     * @param SessionInterface $session
     * @return Response
     */
    public function login(Request $request, SessionInterface $session): Response
    {
        $email = $request->request->get('email');
        $password = $request->request->get('password');

        // set session
        $session->set('email', $email);

        $entries = new DataObject\User\Listing();
        $userList = $entries->load();
        foreach($userList as $userDetails){
            if($email == $userDetails->get('email') && $password == $userDetails->get('password')){
                return $this->redirect("/registerForm");
            }
        }
    }
}