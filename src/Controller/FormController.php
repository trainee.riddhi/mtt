<?php

namespace App\Controller;

use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use \Pimcore\Model\DataObject;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class FormController extends FrontendController
{

    // login form

    /**
     * @Route("/loginform")
     * @param Request $request
     * @return Response
     */
    public function loginform(Request $request): Response
    {
        return $this->render('form/login.html.twig');
    }

    /**
     * @Route("/login")
     * @param Request $request
     * @param SessionInterface $session
     * @return Response
     */
    public function login(Request $request, SessionInterface $session): Response
    {
        $email = $request->request->get('email');
        $password = $request->request->get('password');

        // set session

        $isUserExits = DataObject\User::getByPassword($password, true);

        if ($email == $isUserExits->get('email') && $password == $isUserExits->get('password')) {
            $session->set('password', $password);
            return $this->redirect("/registerForm");
        }

        // $emailSession = $session->get('password');
    }

    // registration form

    /**
     * @Route("/form")
     * @param Request $request
     * @return Response
     */
    public function formAction(Request $request): Response
    {
        $entries = new DataObject\Category\Listing();
        $categoryList = $entries->load();

        $entries = new DataObject\Employee\Listing();
        $employeeList = $entries->load();
        $errorMsg = "";

        return $this->render('form/registration.html.twig', [
            'customString' => $categoryList,
            'empString' => $employeeList,
            'errorMsg' => $errorMsg
        ]);
    }

    /**
     * @Route("/view")
     * @param Request $request
     * @param SessionInterface $session
     * @return Response
     */

    public function view(Request $request, SessionInterface $session): Response
    {

        // p_r($request->request->get('email'));
        // die;

        // $newObject1 = new DataObject\User\Listing();
        // $newObject1 = new DataObject\User::getList();
        // p_r($newObject1->getObjects()[0]->get('key'));
        // die;


        // Example of getting data from the session
        $firstname = $request->request->get('firstname');
        $lastname = $request->request->get('lastname');
        $email = $request->request->get('email');
        $password = $request->request->get('password');
        $phone_number = $request->request->get('phone_number');
        $category = $request->request->get('category');
        $employee = $request->request->all('employee');
        $errorMsg = "";
        if (isset($email) && $email != '') {
            $isUserEmailExits = DataObject\User::getByEmail($email, true);
            if ($isUserEmailExits->get('email') == $email) {
                $errorMsg = "Email already exists";

                return $this->render('form/registration.html.twig', [
                    'errorMsg' => $errorMsg,
                    'customString' => [],
                    'empString' => []
                ]);
            }else {
                $newObject = new DataObject\User();

                $newObject->setKey(\Pimcore\Model\Element\Service::getValidKey('userData4', 'object'));
                $newObject->setParent(DataObject\Service::createFolderByPath("userObjects"));
                // $newObject->setParentId(20);
        
                $newObject->setParent(DataObject\User::getById(36));
        
                $newObject->setFirstname($firstname);
                $newObject->setLastname($lastname);
                $newObject->setEmail($email);
                $newObject->setPassword($password);
                $newObject->setPhoneNumberr($phone_number);
                if ($category) {
                    $categoryObject = DataObject\Category::getById($category);
                    $newObject->setCategory($categoryObject);
                }
                if ($employee) {
                    foreach ($employee as $employees) {
                        $employeeArray[] = DataObject\Employee::getById($employees);
                    }
                    $newObject->setEmpDetails($employeeArray);
                    $array1 = ["red", "black"];
        
                    foreach ($array1 as  $arrays1) {
                        $newObject = new DataObject\User();
                        $newObject->setParent(DataObject\User::getById(42));
                        $newObject->setKey($arrays1);
                        $newObject->setType(DataObject::OBJECT_TYPE_VARIANT);
                        $newObject->setPublished(true);
                        $newObject->save();
                    }
                }
            }
        }
        return new Response(
            'Created',
            Response::HTTP_OK
        );       
        // $newObject = DataObject\User::getById(42);


    }

    /**
     * @Route("/registerForm")
     * @param Request $request
     * @param SessionInterface $session
     * @return Response
     */

    public function registerForm(Request $request, SessionInterface $session): Response
    {

        // p_r($session->get('email'));
        // die;

        if ($session->get('password')) {

            $entries = new DataObject\User\Listing();
            $categoryList = $entries->load();
            p_r($categoryList);
            // $converted_array = (array)$categoryList[1];

            // $blankarray = [];
            // foreach ($categoryList as $entry) {

            //     $blankarray1 = array(
            //         "firstname" => $entry->get("firstname"),
            //         "lastname" => $entry->get("lastname"),
            //         "email" => $entry->get("email"),
            //         "password" => $entry->get("password"),
            //         "phone_number"=>$entry->get("phoneNumber"),
            //     );

            //     array_push($blankarray, $blankarray1);
            // }

            // return $this->render('form/view.html.twig');
            return $this->render('form/view.html.twig', [
                'customString' => $categoryList
            ]);
        } else {
            return $this->redirect("/loginform");
        }
    }

    /**
     * @Route("/test")
     * @param Request $request
     * @return Response
     */

    public function test(Request $request): Response
    {
        return $this->render('form/testform.html.twig');
    }

    /**
     * @Route("/delete/{id}")
     * @param Request $request
     * @return Response
     */

    public function delete($id): Response
    {
        $deleteObject = DataObject\User::getById($id);
        $deleteObject->delete();
        return $this->redirect("/registerForm");
    }

    /**
     * @Route("/update/{id}")
     * @param Request $request
     * @return Response
     */

    public function update($id): Response
    {
        $updateObject = DataObject\User::getById($id);
        return $this->render('form/update.html.twig', [
            'customString' => $updateObject,
        ]);
    }

    /**
     * @Route("/edit/{id}")
     * @param Request $request
     * @return Response
     */

    public function edit(Request $request, $id): Response
    {
        $updateObject = DataObject\User::getById($id);
        $firstname = $request->request->get('firstname');
        $lastname = $request->request->get('lastname');
        $email = $request->request->get('email');
        $phone_number = $request->request->get('phone_number');
        $updateObject->setParentId(20);
        $updateObject->setFirstname($firstname);
        $updateObject->setLastname($lastname);
        $updateObject->setEmail($email);
        $updateObject->setPhoneNumber($phone_number);
        $updateObject->save();
        //  return $this->render("form/update.html.twig");
        return $this->redirect("/registerForm");
    }

    /**
     * @Route("/logout")
     * @param Request $request
     * @param SessionInterface $session
     * @return Response
     */

    public function logout(Request $request, SessionInterface $session): Response
    {
        $session->invalidate();
        return $this->redirect('/loginform');
    }
}
