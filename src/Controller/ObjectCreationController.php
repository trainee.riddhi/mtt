<?php

namespace App\Controller;

use Pimcore\Bundle\AdminBundle\Controller\Admin\LoginController;
use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Pimcore\Model\DataObject;

class ObjectCreationController extends FrontendController
{

    /**
     * @Route("objectCreation", name="opi_page")
     * @param Request $request
     * @return Response
     */

    public function objectCreation(Request $request)
    {
        // Create a new object
        // $newObject->setPublished(true);

        $newObject = new DataObject\Product();
        $newObject->setKey(\Pimcore\Model\Element\Service::getValidKey(uniqid(), 'object'));
        $newObject->setParent(DataObject\Service::createFolderByPath("Product"));
        $newObject->setName("New Name");
        $newObject->setDescription('some text');
        $newObject->save(["versionNote" => "my new version"]);
        return new Response(
            'Created',
            Response::HTTP_OK
        );
    }


    // /**
    //  * @Route("/opi", name="opi_page")
    //  * @param Request $request
    //  * @return Response
    //  */
    // public function indexAction(Request $request): Response
    //  {
    //     die('die');
    //     return $this->render('index/index.html.twig');

    // }
}
