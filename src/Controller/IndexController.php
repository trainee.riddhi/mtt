<?php

namespace App\Controller;

use Pimcore\Bundle\AdminBundle\Controller\Admin\LoginController;
use Pimcore\Bundle\AdminBundle\DataObject\GridColumnConfig\Operator\WorkflowState;
use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Pimcore\Model\DataObject;
use Pimcore\Model\Asset;
use Symfony\Component\Workflow\Workflow;

class IndexController extends FrontendController
{

    /**
     * @Route("/opi", name="opi_page")
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request): Response
    {
        $asset = Asset::getById(746);
        // p_r($asset);
        // die;
        return $this->render('index/index.html.twig');
    }

    /**
     * @Route("/localize", name="localize_page")
     * @param Request $request
     * @return Response
     */
    public function localize(Request $request): Response
    {
        // localization
        $languages = \Pimcore\Tool::getValidLanguages();
        $object = DataObject::getById(17);
        // $languageIndexes = [
        //     'en' => 1,
        //     'fr' => 8,
        //     'de' => 9,
        // ];

        // foreach ($languages as $language) {
        //     $index = $languageIndexes[$language];
        //     $opiObj->setCompanyName($opidata[$index], $language);
        // }
        p_r($languages);
        foreach ($languages as $language) {
            // p_r($language);
            // p_r($object->getName($language));
            if ($language == 'de') {
                $object->setName("Wüsten", $language);
                $object->save();
            }
        }

        // fieldCollection
        $items = new DataObject\Fieldcollection();

        for ($i = 0; $i < 5; $i++) {
            $item = new DataObject\Fieldcollection\Data\MyCollection();
            $item->setCarField("This is a car field" . $i);
            $items->add($item);
        }

        $object->setCollection($items);
        $object->save();

        // die;
        return new Response(
            'Done',
            Response::HTTP_OK
        );
    }

    /**
     * @Route("/Document", name="document_page")
     * @param Request $request
     * @return Response
     */
    public function document(Request $request): Response
    {
        return $this->render('form/document.html.twig');
    }

    /**
     * @Route("/fruitkha", name="fruitkha_page")
     * @param Request $request
     * @return Response
    */
    public function index(Request $request): Response
    {
        return $this->render('fruitkha/index.html.twig');
    }

    /**
     * @Route("/about", name="about_page")
     * @param Request $request
     * @return Response
    */
    public function about(Request $request): Response
    {
        return $this->render('fruitkha/about.html.twig');
    }
}