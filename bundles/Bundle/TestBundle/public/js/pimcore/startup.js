pimcore.registerNS("pimcore.plugin.BundleTestBundle");

pimcore.plugin.BundleTestBundle = Class.create({

    initialize: function () {
        document.addEventListener(pimcore.events.pimcoreReady, this.pimcoreReady.bind(this));
    },

    pimcoreReady: function (e) {
        // alert("BundleTestBundle ready!");
    }
});

var BundleTestBundlePlugin = new pimcore.plugin.BundleTestBundle();
